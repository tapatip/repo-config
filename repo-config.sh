#!/usr/bin/env bash
#
# STEmacsModelines:
# -*- Shell-Unix-Generic -*-
#
# /*!
# @header repo-config.sh
# Update YUM repos to select reachable local mirrors. The repo-config utility
# can be called at system startup to reconfigure YUM to leverage local private
# mirrors and fallback to public mirrors based on server reachability. For an
# additional layer of securirty/reliability a REPO-KEY can be generated and
# added to local mirrors to verify their authenticity but also to verify their
# validity (just because a server is reachable at a particular URL does not
# necessarily mean it is a legitimate mirror). Supports CentOS and Epel repos.
# @author Mark Eissler (mark\@mixtur.com)
# @updated 2014-11-21
# */
#
# To build documentation for this file:
# >headerdoc2html -c doc/headerdoc.cfg -o doc repo-config.sh
#

# Copyright (c) 2014 Mark Eissler, mark@mixtur.com

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PATH=/usr/local/bin

# utility paths (adjust for your local system if needed)
#
# NOTE: Some of these will be updated automatically if OSX is detected.
#
g_path_getopt="/usr/bin/getopt"
g_path_cat="/bin/cat"
g_path_sed="/usr/bin/sed"
g_path_sort="/usr/bin/sort"
g_path_uniq="/usr/bin/uniq"
g_path_tr="/usr/bin/tr"
g_path_mkdir="/bin/mkdir"
g_path_rm="/bin/rm"
g_path_touch="/usr/bin/touch"
g_path_basename="/usr/bin/basename"
g_path_dirname="/usr/bin/dirname"
g_path_pwd="/bin/pwd"
g_path_uname="/usr/bin/uname"
g_path_curl="/usr/bin/curl"
g_path_openssl="/usr/bin/openssl"
g_path_readlink="/usr/bin/readlink"

# path fixups for some systems
[ ! -f "${g_path_sed}" ] && g_path_sed="/bin/sed"
[ ! -f "${g_path_sort}" ] && g_path_sort="/bin/sort"
[ ! -f "${g_path_touch}" ] && g_path_touch="/bin/touch"
[ ! -f "${g_path_basename}" ] && g_path_basename="/bin/basename"
[ ! -f "${g_path_uname}" ] && g_path_uname="/bin/uname"
[ ! -f "${g_path_readlink}" ] && g_path_readlink="/bin/readlink"

###### NO SERVICABLE PARTS BELOW ######
g_version=1.0.4
g_progname=$(${g_path_basename} $0)
g_runos=""

# reset internal vars (do not touch these here)
g_debug=0
g_show_vars=0
g_getopt_old=0
g_forceexec=0
g_defaultIFS=$' \t\n'
g_emptystr=""
g_path_yumconfig="/etc/yum.repos.d/"
g_path_script=""
g_path_scriptdir=""
g_path_templates=""
g_path_infile=""
# caution: outfile must be empty here
g_path_outfile=""
g_use_config=0
g_use_stdout=0
g_repo_staticvars=0
g_path_worktemp="/tmp/rcfg"
g_path_stdout="/dev/stdout"
g_repokey_name="REPO-KEY"

# repo defaults
g_repo_target="CentOS-Base"
g_repo_id="base"
g_repo_name=""
g_repo_baseurl=""
g_repo_mirrorlist=""
g_repo_mirrors=""
g_repo_enable=-1
g_repo_target_cli=""
g_repo_target_ini=""
# yum vars (normally set by YUM dynamically)
g_repo_basearch="x86_64"
g_repo_releasever="7"
g_repo_basearch_cli=""
g_repo_basearch_ini=""
g_repo_releasever_cli=""
g_repo_releasever_ini=""

# min bash required
g_vers_bash_major=4
g_vers_bash_minor=2
g_vers_bash_patch=0

# check for minimum bash
if [[ ${BASH_VERSINFO[0]} < ${g_vers_bash_major} ||
  ${BASH_VERSINFO[1]} < ${g_vers_bash_minor} ||
  ${BASH_VERSINFO[2]} < ${g_vers_bash_patch} ]]; then
  echo -n "${PROGNAME} requires at least BASH ${g_vers_bash_major}.${g_vers_bash_minor}.${g_vers_bash_patch}!"
  echo " (I seem to be running in BASH ${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}.${BASH_VERSINFO[2]})"
  echo
  exit 100
fi

#
# FUNCTIONS
#

# getRealPath()
# /*!
# @abstract Determine the real path to a file or directory
# @discussion
# Traverse symbolic links to discover the real physical path of a file or
#   directory.
# @param target Target file or directory
# @return Returns string containing real path (status 0) or an empty string
#   (status 1) on failure.
# */
getRealPath() {
  local target="${1}"
  local _collisionGuard=0
  local _collisionGuardMax=10
  local _physicalDir _realPath
  local _dirname _basename

  if [[ -z "${target}" ]]; then
    echo ""; return 1
  fi

  # unwind trail of of symlinks until we reach the underlying file/directory
  while [[ -L "${target}" && ${_collisionGuard} -lt ${_collisionGuardMax} ]]; do
    # cd into target directory
    _dirname=$(getDirName "${target}")
    cd "${_dirname}"
    # resolve target link
    _basename=$(getBaseName "${target}")
    target=$(${g_path_readlink} "${_basename}")
    (( _collisionGuard++ ))
  done

  if [[ ${_collisionGuard} -eq ${_collisionGuardMax} ]]; then
    echo ""; return 1
  fi

  # compute canonicalized path by combining resolved directory and target name
  _dirname=$(getDirName "${target}")
  _physicalDir=$(cd "${_dirname}" && pwd -P)
  if [ -d ${target} ]; then
    _basename="${target}"
  else
    _basename=$(getBaseName "${target}")
  fi
  _realPath="${_physicalDir}/${_basename}"

  echo "${_realPath}"; return 0
}

# getBaseName()
# /*!
# @abstract Determine the basename of a file or directory path
# @discussion
# Parse a file or directory path and extract the basename part.
# @param target Target file or directory
# @return Returns string containing basename (status 0) or an empty string
#   (status 1) on failure.
# */
getBaseName() {
  local target="${1}"
  local _basename

  if [[ -z "${target}" ]]; then
    echo ""; return 1
  fi

  _basename="${target##*/}"

  echo "${_basename}"; return 0
}

# getDirName()
# /*!
# @abstract Determine the directory of a file or directory path
# @discussion
# Parse a file or directory path and extract the directory part.
# @param target Target file or directory
# @return Returns string containing directory (status 0) or an empty string
#   (status 1) on failure.
# */
getDirName() {
  local target="${1}"
  local _dirname

  if [[ -z "${target}" ]]; then
    echo ""; return 1
  fi

  shopt -s extglob;
  _dirname="${target%%?(+([^/]))?([/])}";
  _dirname="${_dirname:-.}";
  shopt -u extglob;

  # deal with trailing slash
  if [[ ${_dirname} =~ [^/]+/$ ]]; then
    _dirname="${_dirname%/}"
  fi

  echo "${_dirname}"; return 0
}

# getRunOS()
# /*!
# @abstract Determine running OS
# @discussion
# Use strategies to determine which OS the current script is running on. One
#   the following strings will be returned:
#   - linux
#   - freebsd
#   - osx
#   - solaris
#   - unknown
# @return Returns string identifying OS or "unknown" (status 0) or "unavail"
#   (status 1) on failure.
# */
getRunOS() {
  local _runos="unknown"
  local _unamestr
  declare -A osconfig

  if [ ! -x ${g_path_uname} ]; then
    unset osconfig
    echo "unavail"; return 1
  fi

  _unamestr=$(${g_path_uname})

  case "${_unamestr}" in
  "Linux" )
    _runos="linux"
    if [ -f /etc/os-release ]; then
      # fetch osconfig
      readConfigFromFile osconfig "/etc/os-release"
      if [[ "${#osconfig[@]}" -gt 0 ]]; then
        _runos="${_runos},${osconfig[NAME]},${osconfig[VERSION]}"
      else
        _runos="${_runos},,"
      fi
    elif [ -f /etc/arch-release ]; then
      # /etc/arch-release is an empty file
      _runos="${_runos},Arch Linux,"
    elif [ -f /etc/gentoo-release ]; then
      # older gentoo may still have this file
      osconfig[NAME]=$(${g_path_cat} /etc/gentoo-release \
        | ${g_path_sed} -En -e 's|^([A-Za-z ]*)*release ([0-9\.]*.*)*$|\1|p')

      osconfig[VERSION]=$(${g_path_cat} /etc/gentoo-release \
        | ${g_path_sed} -En -e 's|^([A-Za-z ]*)*release ([0-9\.]*.*)*$|\2|p')

      _runos="${_runos},${osconfig[NAME]},${osconfig[VERSION]}"
    elif [ -f /etc/fedora-release ]; then
      # older fedora may still have this file
      osconfig[NAME]=$(${g_path_cat} /etc/fedora-release \
        | ${g_path_sed} -En -e 's|^([A-Za-z ]*)*release ([0-9\.]*.*)*$|\1|p')

      osconfig[VERSION]=$(${g_path_cat} /etc/fedora-release \
        | ${g_path_sed} -En -e 's|^([A-Za-z ]*)*release ([0-9\.]*.*)*$|\2|p')

      _runos="${_runos},${osconfig[NAME]},${osconfig[VERSION]}"
    elif [ -f /etc/redhat-release ]; then
      osconfig[NAME]=$(${g_path_cat} /etc/redhat-release \
        | ${g_path_sed} -En -e 's|^([A-Za-z ]*)*release ([0-9\.]*.*)*$|\1|p')

      osconfig[VERSION]=$(${g_path_cat} /etc/redhat-release \
        | ${g_path_sed} -En -e 's|^([A-Za-z ]*)*release ([0-9\.]*.*)*$|\2|p')

      _runos="${_runos},${osconfig[NAME]},${osconfig[VERSION]}"
    elif [ -f /etc/debian_version ]; then
      osconfig[NAME]="Debian GNU/Linux"
      osconfig[VERSION]=$(${g_path_cat} /etc/debian_version)

      _runos="${_runos},${osconfig[NAME]},${osconfig[VERSION]}"
    else
      _runos="unknown,Unknown,"
    fi
    ;;
  "FreeBSD" )
    _runos="freebsd,FreeBSD,"
    ;;
  "Darwin" )
    _runos="darwin,OSX,"
    ;;
  "SunOS" )
    _runos="solaris,Solaris,"
    ;;
  * )
    ;;
  esac

  unset osconfig

  echo "${_runos}"; return 0
}

# getRunOSName()
# /*!
# @abstract Extract the name portion out of a runos string
# @discussion
# Extracts the name portion from a runos string (as returned by getRunOS()) by
# stripping the first and last parts.
# @param runOSString A runos string
# @return Returns string containin OS name or "unavail" (status 1) on failure
# */
getRunOSName() {
  local runOSString="${1}"
  local runOSName="unavail"

  if [ -z "${runOSString}" ]; then
    echo ""; return 1
  fi

  # runos pattern: linux,CentOS,7
  #
  # strip first part, then last part, to extract the middle
  #
  _runOSName="${runOSString#*,}"; _runOSName="${_test%,*}";

  echo "${_runOSName}"; return 0
}

# usage()
# /*!
# @abstract Output correct help depending on installed getopt() functionality
# @discussion
# Installed version of getopt utility may support short options or long and
#   short options. Calling this function will output the appropriate usage info.
#   The global "$g_getopt_old" variable must be defined.
# */
function usage {
  if [ ${g_getopt_old} -eq 1 ]; then
    echo "usage: ${g_progname} [-d] [options] [-c config-file]"
    echo "usage: ${g_progname} [-d] -h | -k | -v"
  else
    echo "usage: ${g_progname} [-d|--debug] [options] [-c config-file]"
    echo "usage: ${g_progname} [-d|--debug] -h | --help"
    echo "usage: ${g_progname} [-d|--debug] -k | --new-repokey"
    echo "usage: ${g_progname} [-d|--debug] -v | --version"
  fi

  echo
}

# help()
# /*!
# @abstract Output correct help depending on installed getopt() functionality
# @discussion
# Installed version of getopt utility may support short options or long and
#   short options. Calling this function will output the appropriate usage info.
#   The global "$g_getopt_old" variable must be defined.
# */
function help {
  if [ ${g_getopt_old} -eq 1 ]; then
    help_old
  else
    help_new
  fi
}

# help_new()
# /*!
# @abstract Output verbose usage info that includes long option flags
# @discussion
#   This method shouldn't be called directly. Call help() instead.
# @internal
# */
function help_new {
usage

${g_path_cat} << EOF
Generate YUM repository configuration files and configure for first reachable
local mirror (if defined and) if reachable, fallback to baseurl and mirrorlist.

OPTIONS:
   -c, --use-config             Parse config file containing keys/vals
   -i, --in-file=path           Input file (path) instead of default
   -o, --out-file=path          Output file (path) instead of default
   -b, --repo-baseurl=URL       URL pointing to a baseurl
   -m, --repo-mirrorlist=URL    URL pointing to a mirrorlist server
   -n, --repo-id=name           Globally unique repo (section) id
                                  - "base"
                                  - "updates"
                                  - "extras"
                                  - "centosplus"
                                  - "base-source"
                                  - "updates-source"
                                  - "extras-source"
                                  - "centosplus-source"
                                  - "epel"
                                  - "epel-debuginfo"
                                  - "epel-source"
   -l, --repo-name=name         Human readable string describing the repo
   -p, --private-mirrors=list   Comma separated list of private mirrors
   -t, --target=name            Parent repo target:
                                  - "CentOS-Base" (default)
                                  - "CentOS-Sources"
                                  - "epel"
   -u, --update-yum             Update system files (on supported OS)
   -x, --repo-disable           Disable repo
   -y, --repo-enable            Enable repo
   -a, --repo-basearch          Replacement for YUM \$basearch variable:
                                  - "i386"
                                  - "x86_64"
   -r, --repo-releasever        Replacement for YUM \$releasever variable
   -s, --repo-staticvars        Generate output files with static values
   -k, --new-repokey            Generate a new repokey (REPO-KEY)
   -d, --debug                  Turn debugging on (increases verbosity)
   -f, --force                  Execute without user prompt
   -h, --help                   Show this message
   -v, --version                Output version of this script
   -z, --show-settings          Show final settings that will be applied

Viewing merged settings (-z, --show-settings)
---------------------------------------------
Merged settings can be viewed without creating any file output by specifying the
show settings option. If in doubt, perform a dry run with this option enabled.

Use config file (-c, --use-config)
----------------------------------
When the config file option is enabled you must specify a config file to parse
as the final argument or use redirection as input.

Input and Output files (-i, --in-file; -o, --out-file)
------------------------------------------------------
An alternative base YUM configuration template file can be specified (with the
--in-file option) and features tokens that will be replaced by default values
or parameters passed to this script. If no input alternative input file is
specified, the default input file will be used. Output will be to the default
output path if no output file is specified (with the --out-file option) and
the system update option has not been enabled (with the --update-yum option).

Specifying repo options
-----------------------
When specifying repo options oon the cli keep in mind that you can only affect
one repo target file and within that target only one section.

Static YUM variable replacement
-------------------------------
Repo URLs often have the YUM \$basearch and/or \$releasever variables embedded.
This is problematic for testing mirror reachability. Static replacement values
for these variables are defined as defaults but they can also be defined in the
config file or on the command line (--repo-basearch and --repo-releasever).

Under normal operation the values will only be used to test reachability for
repo mirrors; however enabling the static vars option (--repo-staticvars) will
result in the generation of YUM configuration files with static values.

Updating system files (-u, --update-yum)
----------------------------------------
On supported operating systems (i.e. CentOS), YUM configuration files will be
updated in place when the --update-yum option is used. On unsupported systems
the option will be ignored and output will be to stdout.

Parameter value priority
------------------------
In general, values passed as cli parameters take precedence over those passed in
an input-file; values passed in an input file take precedence over those defined
as defaults: cli->ini->defaults

EOF
}

# help_old()
# /*!
# @abstract Output help info that does not include long option flags
# @discussion
#   This method shouldn't be called directly. Call help() instead.
# @internal
# */
function help_old {
usage

${g_path_cat} << EOF
Generate YUM repository configuration files and configure for first reachable
local mirror (if defined and) if reachable, fallback to baseurl and mirrorlist.

OPTIONS:
   -c                           Parse config file containing keys/vals
   -i                           Input file (path) instead of default
   -o                           Output file (path) instead of default
   -b                           URL pointing to a baseurl
   -m                           URL pointing to a mirrorlist server
   -n                           Globally unique repo (section) id
                                  - "base"
                                  - "updates"
                                  - "extras"
                                  - "centosplus"
                                  - "base-source"
                                  - "updates-source"
                                  - "extras-source"
                                  - "centosplus-source"
                                  - "epel"
                                  - "epel-debuginfo"
                                  - "epel-source"
   -l                           Human readable string describing the repo
   -p                           Comma separated list of private mirrors
   -t                           Parent repo target:
                                  - "CentOS-Base" (default)
                                  - "CentOS-Sources"
                                  - "epel"
   -u                           Update system files (on supported OS)
   -x                           Disable repo
   -y                           Enable repo
   -a                           Replacement for YUM \$basearch variable:
                                  - "i386"
                                  - "x86_64"
   -r                           Replacement for YUM \$releasever variable
   -s                           Generate output files with static values
   -k                           Generate a new repokey (REPO-KEY)
   -d                           Turn debugging on (increases verbosity)
   -f                           Execute without user prompt
   -h                           Show this message
   -v                           Output version of this script
   -z                           Show final settings that will be applied

Viewing merged settings (-z)
----------------------------
Merged settings can be viewed without creating any file output by specifying the
show settings option. If in doubt, perform a dry run with this option enabled.

Use config file (-c)
--------------------
When the config file option is enabled you must specify a config file to parse
as the final argument or use redirection as input.

Input and Output files (-i; -o)
-------------------------------
An alternative base YUM configuration template file can be specified (with the
-i option) and features tokens that will be replaced by default values or
parameters passed to this script. If no input alternative input file is
specified, the default input file will be used. Output will be to the default
output path if no output file is specified (with the -o option) and the system
update option has not been enabled (with the -u option).

Specifying repo options
-----------------------
When specifying repo options oon the cli keep in mind that you can only affect
one repo target file and within that target only one section.

Static YUM variable replacement
-------------------------------
Repo URLs often have the YUM \$basearch and/or \$releasever variables embedded.
This is problematic for testing mirror reachability. Static replacement values
for these variables are defined as defaults but they can also be defined in the
config file or on the command line (-a and -r).

Under normal operation the values will only be used to test reachability for
repo mirrors; however enabling the static vars option (-s) will result in the
generation of YUM configuration files with static values.

Updating system files (-u)
--------------------------
On supported operating systems (i.e. CentOS), YUM configuration files will be
updated in place when the -u option is used. On unsupported systems the option
will be ignored and output will be to stdout.

Parameter value priority
------------------------
In general, values passed as cli parameters take precedence over those passed in
an input-file; values passed in an input file take precedence over those defined
as defaults: cli->ini->defaults

EOF
}

# getEscapedStr()
# /*!
# @abstract Escape special characters in a string
# @discussion
# Parse a string and escape all special characters (listed below) with a back
#   slash character. The current list of characters to be escaped are:
#
#     # - (pound)
#     $ - (dollar)
#     % - (percent)
#     & - (ampersand)
#     _ - (underscore)
#     \ - (back slash)
#
#   Sed has problems digesting both the ampersand (&) and the dollar ($) chars.
#   To prevent problems, run your string through this function BUT then also
#   pay attention to how the string is used so that escape characters are not
#   stripped inadvertently after conversion. When in doubt, assign the value to
#   another variable and wrap it in (set -x .... set +x sequences).
# @param string A string to be escaped
# @return Generated string (status 0) or empty string (status 1) on failure.
# */
getEscapedStr() {
  local string="${1}"
  local _escapedString=""

  if [ -z "${string}" ]; then
    echo ""; return 1
  fi

  # escape characters: #,$,%,&,_,\
  _escapedString=$(echo ${string} | ${g_path_sed} -e 's/[#$%&_\\ ]/\\&/g')

  echo ${_escapedString}; return 0
}

# getUniqStr()
# /*!
# @abstract Get a random string
# @discussion
# Generates a 5-character random string or the specified length.
# @param length Length of string to generate.
# @return Generated string.
# */
getUniqStr() {
  local uniqstr
  local length=5

  if [ -n "${1}" ]; then
    length=${1}
  fi

  dict="abcdefghijkmnopqrstuvxyz123456789"
  dictlen="${#dict}"
  for i in $(eval echo {1..$length}); do
    rand=$(( $RANDOM%${dictlen} ))
    uniqstr="${uniqstr}${dict:$rand:1}"
  done

  echo $uniqstr
}

# getRepoKey()
# /*!
# @abstract Generate a new repo key
# @discussion
# Generates a new repo key which is a SHA1 encrypted string whose input is a
# random string of 16 characters. The repo key can be uploaded to a repo mirror
# for use as a REPO-KEY. When used as a REPO-KEY, the key should be uploaded to
# the repo root directory or one of the subdirectory parts provided in the repo
# mirror url (each subdirectory will be checked for the key).
# @return Generated string (status 0) or empty string (status 1) on failure.
# */
getRepoKey() {
  local _randomStr=$(getUniqStr 16)
  local _sha1=$(echo -n ${_randomStr} | ${g_path_openssl} sha1 -hmac "key")

  if [ $? -ne 0 ]; then
    echo ""; return 1
  fi

  # on some systems we need to strip a leading "(stdin)= " string, so we just
  # strip everything up to the first space.
  echo ${_sha1/* }; return 0
}

# getUrlHost()
# /*!
# @abstract Parse an URL string and extract the host component
# @discussion
# Extract the host part from an URL string. The extracted string will include
#   the protocol, and if no protocol is provided, the http:// protocol will be
#   prepended to the returned string. A trailing forward slash will also be
#   appended to the returned string (if no trailing slash is present).
# @param urlString An URL string to be parsed
# @return Returns URL host string (status 0) or an emnpty string (status 1) on
#   failure.
# */
getUrlHost() {
  local urlString="${1}"
  local _urlHost=""
  local _urlProtoDefault="http://"

  if [ -z "${urlString}" ]; then
    echo ""; return 1
  fi

  _urlHost="$(echo ${urlString} \
    | ${g_path_sed} -En -e 's|^(http://\|https://)?([[:alnum:].:-]+){1}(.*)$|\1\2|p')"

  # make sure url starts with a protocol, or we add http://
  if [[ ! "${_urlHost}" =~ ^(http://|https://) ]]; then
    _urlHost="${_urlProtoDefault}${_urlHost}"
  fi

  # add trailing slash
  _urlHost+="/"

  echo "${_urlHost}"; return 0
}

# getUrlPath()
# /*!
# @abstract Parse an URL string and extract the path component
# @discussion
# Extract the path component part from an URL string. The extracted string will
#   include a leading forward slash.
# @param urlString An URL string to be parsed
# @return Returns URL path string (status 0) or an emnpty string (status 1) on
#   failure.
# */
getUrlPath() {
  local urlString="${1}"
  local _urlPath=""

  if [ -z "${urlString}" ]; then
    echo ""; return 1
  fi

  _urlPath="$(echo ${urlString} \
    | ${g_path_sed} -En -e 's|^(http://\|https://)?([[:alnum:].:-]+){1}(.*)$|\3|p')"

  echo "${_urlPath}"; return 0
}

# getUrlStaticPath()
# /*!
# @abstract Replace (YUM) variables in url path to produce a static url
# @discussion
# The following variables will be replaced in the urlString with static values
#   set in global variables indicated in brackets:
#     - $basearch (g_repo_basearch)
#     - $releasever (g_repo_releasever)
# @param urlString An URL string to be parsed
# @return Returns an URL string (status 0) or an empty string (status 1) on
#   failure.
getUrlStaticPath() {
  local urlString="${1}"
  local _staticUrlString=""

  if [ -z "${urlString}" ]; then
    echo ""; return 1
  fi

  # replace $basearch, $releasever
  _staticUrlString="$(echo ${urlString} \
    | ${g_path_sed} -e 's|\$basearch|'${g_repo_basearch}'|g' \
      -e 's|\$releasever|'${g_repo_releasever}'|g')"

  if [ $? -ne 0 ]; then
    echo ""; return 1
  fi

  echo "${_staticUrlString}"; return 0
}

# mkTempFile()
# /*!
# @abstract Create a temporary filename
# @discussion
# The filename will be prepended with the "/tmp" if a directory is not specified
#   and the current directory has not been indicated. The generated filename
#   will be prepended with the process PID if a filename prefix is not specified
#   and the option to omit the pid has not been indicated. If calling this
#   function recursively, specify all parameters including the collision guard,
#   an integer that is incremented on each pass to avoid race condtions.
#
#   By default, mkTempDir() operates in safe mode and will create the underlying
#   directory; this behavior can be disabled by declaring the TMPMK global
#   variable and setting it to a value other than zero. This can be done
#   temporarily like this:
#       $(TMPMK=0 mkTempDir)
#   In the above example, a subshell is spawned but this is not necessary. Of
#   course, you can also just set TMPMK=0 at the global level of your script.
# @param tempDir The directory or "-" to use the current directory.
# @param prependString A string to prepend (a "prefix") or "-" to omit the PID.
# @param collisionGuard Collision guard
# @return Generated filename (status 0) or empty string (status 1) on failure.
# */
function mkTempFile() {
  local tempFile=""
  local tempDir=""
  if [ "${1}" != "-" ]; then
    tempDir=${1}${1:+/}
    tempDir=${tempDir:-/tmp/}
  fi

  local collisionGuard=0
  if [ -n "${3}" ] && [ "${3}" -gt 0 ]; then
    collisionGuard=${3}
  fi

  while [[ $collisionGuard -lt 5 ]]; do
    if [ "${2}" != "-" ]; then
      tempFile=${2}${2:+-}
      local tempPid=$$
      tempFile=${tempFile:-${tempPid}-}
    fi
    tempFile=$tempDir$tempFile$RANDOM$RANDOM.tmp
    if [ -e $tempFile ]; then
      # if file exists, try again
      tempFile=$(mkTempFile $1 $2 $collisionGuard)
      if [ $? -ne 0 ]; then
        (( collisionGuard++ ))
        continue
      fi
    fi
    if [[ -z "${TMPMK}" || ${TMPMK} -ne 0 ]]; then
      ${g_path_mkdir} -p "${tempDir}"
      ${g_path_touch} "${tempFile}"
    fi
    echo ${tempFile}; return 0
  done

  echo ""; return 1
}

# mkTempDir()
# /*!
# @abstract Create tempory directory name
# @discussion
# The directory name will be prepended with the "/tmp" if a directory is not
#   specified and the current directory has not been indicated. The generated
#   directory name will be prepended with the process PID if a filename prefix
#   is not specified and the option to omit the pid has not been indicated. If
#   calling this function recursively, specify all parameters including the
#   collision guard, an integer that is incremented on each pass to avoid race
#   condtions.
#
#   By default, mkTempDir() operates in safe mode and will create the underlying
#   directory; this behavior can be disabled by declaring the TMPMK global
#   variable and setting it to a value other than zero. This can be done
#   temporarily like this:
#       $(TMPMK=0 mkTempDir)
#   In the above example, a subshell is spawned but this is not necessary. Of
#   course, you can also just set TMPMK=0 at the global level of your script.
# @param prependString A string to prepend (a "prefix") or "-" to omit the PID.
# @param collisionGuard Collision guard
# @return Generated filename (status 0) or empty string (status 1) on failure.
# */
function mkTempDir() {
  local tempDir=""
  if [ "${1}" != "-" ]; then
    tempDir=${1}${1:+/}
    tempDir=${tempDir:-/tmp/}
  fi

  local collisionGuard=0
  if [ -n "${3}" ] && [ "${3}" -gt 0 ]; then
    collisionGuard=${3}
  fi

  while [[ $collisionGuard -lt 5 ]]; do
    if [ "${2}" != "-" ]; then
      tempSubDir=${2}${2:+-}
      local tempPid=$$
      tempDir=${tempDir}${tempSubDir:-${tempPid}-}
    fi

    tempDir=$tempDir$RANDOM$RANDOM
    if [ -d $tempDir ]; then
      # if directory exists, try again
      tempDir=$(mkTempDir $1 $2 $collisionGuard)
      if [ $? -ne 0 ]; then
        (( collisionGuard++ ))
        continue
      fi
    fi

    if [[ -z "${TMPMK}" || ${TMPMK} -ne 0 ]]; then
      ${g_path_mkdir} -p "${tempDir}"
    fi
    echo ${tempDir}; return 0
  done

  echo ""; return 1
}

# promptConfirm()
# /*!
# @abstract Confirm a user action. Input case insensitive
# @discussion
# Read a simple case-insensitive [y]es or [N]o prompt, where "No" is the default
#   if no answer is entered by the user. This function does not present a
#   prompt, it only reads the response.
# @return Returns string containiner "yes" or "no".
# */
function promptConfirm() {
  read -p "$1 ([y]es or [N]o): "
  case $(echo $REPLY | ${g_path_tr} '[A-Z]' '[a-z]') in
    y|yes) echo "yes" ;;
    *)     echo "no" ;;
  esac
}

# version()
# /*!
# @abstract Output version information
# @discussion
# Output a version string on the first line, any other version information on
#   additional lines.
# */
function version {
  echo ${g_progname} ${g_version};
}

# isNumber()
# /*!
# @abstract Check if a given string is a number
# @discussion
# Parses a string to test whether its components are digits including decimals.
#   The string can be preceded by a negative sign to indicate a negative value.
# @param numberStr String to check.
# @return Returns 1 on success (status 0), 0 on failure (status 1).
# */
isNumber() {
  if [[ ${1} =~ ^-?[0-9]+([.][0-9]+)*$ ]]; then
    # match
    echo 1; return 0
  else
    # no match
    echo 0; return 1
  fi
}

# isPathWriteable()
# /*!
# @abstract Checks if a given path is writeable by the current user
# @discussion
# Tests whether a given path (file or directory) is writable by the current
#   user.
# @return Returns 1 on success (status 0), 0 on failure (status 1).
# */
isPathWriteable() {
  if [ -z "${1}" ]; then
    echo 0; return 1
  fi

  # path is a directory...
  if [[ -d "${1}" ]]; then
    local path="${1%/}/.test"
    local resp rslt
    resp=$({ ${g_path_touch} "${path}"; } 2>&1)
    rslt=$?
    if [[ ${rslt} -ne 0 ]]; then
      # not writeable directory
      echo 0; return 1
    else
      # writeable directory
      ${g_path_rm} "${path}"
      echo 1; return 0
    fi
  fi

  # path is a file...
  if [ -w "${1}" ]; then
    # writeable file
    echo 1; return 0
  else
    # not writeable file
    echo 0; return 1
  fi

  # and if we fall through...
  echo 0; return 128
}

# readConfigFromArray()
# /*!
# @abstract Parse an array of KEY=VAL strings into an assoc array
# @discussion
# Iterates over each row of the array, parses the KEY=VAL definitions contained
#   in the strings, and writes an associative array to a global var passed by
#   name in outputVarName.
#   The outputVarName must not be quoted by the caller:
#       readConfigFromArray myOutputVar inputArray[\@]
#
#   In the above example, the caller can then access values:
#       ${myOutputVar[KEY]}
#
#   Sections are supported and follow the [SECTION_NAME] syntax. Section names
#   must appear on lines by themselves and precede the KEY=VAL definictions that
#   they contain. Section names will be converted to uppercase. For flexibility,
#   a pseudo-multidimensional associative array is created by using the
#   following key format:
#       [SECTION_NAME,KEY]=VAL
#
#   By default, the SECTION_NAME and KEY name parts will be delimited with a
#   comma as shown above; however, an alternate character (or string) can be set
#   as a delimiter by declaring the INIDM global variable and setting it to the
#   preferred value. This can be done temporarily like this:
#       INIDM='_' readConfigFromArray myOutputVar /path/to/file
#
#   Of course, you can also just set INIDM at the global level of your script.
#
#   All KEY=VAL definitions that should appear outside of any section (that is,
#   at the global level) must appear before any section name is encountered.
#
#   Neither [SECTION_NAME] or KEY=VAL definitions can be followed by comments.
#
# @param outputVarName Name of global variable into which output will be copied.
# @param inputArray Array containing strings of KEY=VAL pairs.
# */
readConfigFromArray() {
  local _outputVarName="$1"
  local _inputArray=("${!2}")
  local _inputArrayLen="${#_inputArray[@]}"
  local _key _val
  local _assignOp='='
  local _lineRegex="^[^#;]*[^${_assignOp}][${_assignOp}]{1}[^${_assignOp}]*$"
  # assoc array (i.e. section label) support
  local _sectionNameBeg='['
  local _sectionNameEnd=']'
  local _lineRegexArr="^[${_sectionNameBeg}]{1}[[:alnum:]\_\-]+[${_sectionNameEnd}]{1}$"
  local _sectionNameKey=""
  local _sectionNameKeyDelim=','
  local _count=0
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}
  declare -g -A "$_outputVarName"
  if [[ ! -z ${INIDM+x} ]]; then
    _sectionNameKeyDelim="${INIDM}"
  fi
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG ($FUNCNAME): _inputArray length: ${_inputArrayLen}"
  fi
  for ((i=0; i<${_inputArrayLen}; i++)); do
    _line="${_inputArray[$i]}"
    if [[ "${_line}" =~ ${_lineRegexArr} ]]; then
      # convert label to uppercase, extract part between square brackets
      local __label="${_line^^}"; __label=${__label:1:-1}
      _sectionNameKey=$__label
      unset __label
    fi
    # assume comments may not be indented
    if [[ "${_line}" =~ ${_lineRegex} ]]; then
      IFS=${_assignOp} read _key _val <<< "$_line"
      [[ -n $_key ]] || continue
      # convert key to uppercase
      _key="${_key^^}"
      # add array label to key
      if [[ -n "${_sectionNameKey}" ]]; then
        _key="${_sectionNameKey}${_sectionNameKeyDelim}${_key}"
      fi
      if [[ ${g_debug} -ne 0 ]]; then
        (( _count++ ))
        printf "DEBUG ($FUNCNAME): %02s [${_key}]: ${_val}\n" "${_count}"
      fi
      # globally replace '$' char in value with '\$'
      _val=${_val//\$/\\$}
      # remove leading and trailing quotes
      _val=${_val#\"}; _val=${_val%\"}
      eval "IFS=${_assignOp} $_outputVarName[${_key}]=\"${_val}\""
    fi
  done
}

# readConfigFromFile()
# /*!
# @abstract Parse a config file with KEY=VAL definitions into an assoc array
# @discussion
# Reads the KEY=VAL definitions from the file pointed to by inputFilePath and
#   writes an associative array to a global var passed by name in outputVarName.
#   The outputVarName must not be quoted by the caller:
#       readConfigFromFile myOutputVar /path/to/file
#
#   In the above example, the caller can then access values:
#       ${myOutputVar[KEY]}
#
#   Sections are supported and follow the [SECTION_NAME] syntax. Section names
#   must appear on lines by themselves and precede the KEY=VAL definictions that
#   they contain. Section names will be converted to uppercase. For flexibility,
#   a pseudo-multidimensional associative array is created by using the
#   following key format:
#       [SECTION_NAME,KEY]=VAL
#
#   By default, the SECTION_NAME and KEY name parts will be delimited with a
#   comma as shown above; however, an alternate character (or string) can be set
#   as a delimiter by declaring the INIDM global variable and setting it to the
#   preferred value. This can be done temporarily like this:
#       INIDM='_' readConfigFromFile myOutputVar /path/to/file
#
#   Of course, you can also just set INIDM at the global level of your script.
#
#   All KEY=VAL definitions that should appear outside of any section (that is,
#   at the global level) must appear before any section name is encountered.
#
#   Neither [SECTION_NAME] or KEY=VAL definitions can be followed by comments.
# @param outputVarName Name of global variable into which output will be copied.
# @param inputFilePath Path to input file.
# */
readConfigFromFile() {
  local _outputVarName="$1"
  local _inputFilePath="$2"
  local _key _val
  # line key/val support
  local _assignOp='='
  local _lineRegex="^[^#;]*[^${_assignOp}][${_assignOp}]{1}[^${_assignOp}]*$"
  # assoc array (i.e. section label) support
  local _sectionNameBeg='['
  local _sectionNameEnd=']'
  local _lineRegexArr="^[${_sectionNameBeg}]{1}[[:alnum:]\_\-]+[${_sectionNameEnd}]{1}$"
  local _sectionNameKey=""
  local _sectionNameKeyDelim=','
  local _count=0
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}
  declare -g -A "$_outputVarName"
  if [[ ! -z ${INIDM+x} ]]; then
    _sectionNameKeyDelim="${INIDM}"
  fi
  if [ -n "${_inputFilePath}" ]; then
    while IFS= read _line; do
      if [[ "${_line}" =~ ${_lineRegexArr} ]]; then
        # convert label to uppercase, extract part between square brackets
        local __label="${_line^^}"; __label=${__label:1:-1}
        _sectionNameKey=$__label
        unset __label
      fi
      # assume comments may not be indented
      if [[ "${_line}" =~ ${_lineRegex} ]]; then
        IFS=${_assignOp} read _key _val <<< "$_line"
        [[ -n $_key ]] || continue
        # convert key to uppercase
        _key="${_key^^}"
        # add array label to key
        if [[ -n "${_sectionNameKey}" ]]; then
          _key="${_sectionNameKey}${_sectionNameKeyDelim}${_key}"
        fi
        if [[ ${g_debug} -ne 0 ]]; then
          (( _count++ ))
          printf "DEBUG ($FUNCNAME): %02s [${_key}]: ${_val}\n" "${_count}"
        fi
        # globally replace '$' char in value with '\$'
        _val=${_val//\$/\\$}
        # remove leading and trailing quotes
        _val=${_val#\"}; _val=${_val%\"}
        eval "IFS=${_assignOp} $_outputVarName[${_key}]=\"${_val}\""
      fi
    done <<< "$(${g_path_cat} ${_inputFilePath})"
  else
    while IFS= read reply; do
      if [[ "${_line}" =~ ${_lineRegexArr} ]]; then
        # convert label to uppercase, extract part between square brackets
        local __label="${_line^^}"; __label=${__label:1:-1}
        _sectionNameKey=$__label
        unset __label
      fi
      # assume comments may not be indented
      if [[ "${_line}" =~ ${_lineRegex} ]]; then
        IFS=${_assignOp} read _key _val <<< "$_line"
        [[ -n $_key ]] || continue
        # convert key to uppercase
        _key="${_key^^}"
        # add array label to key
        if [[ -n "${_sectionNameKey}" ]]; then
          _key="${_sectionNameKey}${_sectionNameKeyDelim}${_key}"
        fi
        if [[ ${g_debug} -ne 0 ]]; then
          (( _count++ ))
          printf "DEBUG ($FUNCNAME): %02s [${_key}]: ${_val}\n" "${_count}"
        fi
        # globally replace '$' char in value with '\$'
        _val=${_val//\$/\\$}
        # remove leading and trailing quotes
        _val=${_val#\"}; _val=${_val%\"}
        eval "IFS=${_assignOp} $_outputVarName[${_key}]=\"${_val}\""
      fi
    done
  fi
}

# parseConfigSections() {
# /*!
# @abstract Extract a list of unique section names from config assoc array
# @discussion
# Iterate over the associative array passed by name in inputAssocArray, extract
#   all keys, sort and deduplicate the list of keys then store it in the array
#   passed by name in outputVarName.
#
#   Neither the outputVarName or the inputAssocArray must be quoted by the
#   caller. The array names may be passed with the array [@] syntax in place
#   for clarity:
#       parseConfigSections myOutputVar[@] myInputArray[@]
#
#   In the above example, the caller can then access values as a regular single
#   dimensional array.
#       ${myOutputVar[0]}
#
#   Output all values:
#       ${myOutputVar[@]}
# @param outputVarName Name of global variable into which output will be copied.
# @param inputAssocArray Associative array whose keys should be deduped.
# */
parseConfigSections() {
  # strip the [@] part if supplied with input array names
  local _outputVarName="${1%%[@\]}"
  local _inputAssocArray="${2%%[@\]}"
  local _nonuniqSections=()
  local _uniqSections=()
  local _section
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}

  # FIXME: There doesn't seem to be any need to use these "declare" statements.
  #
  # declare -g -A "$_inputAssocArray"
  # declare -g -a "$_outputVarName"

  # extract all sections from _inputArray (an assoc array) keys
  eval '_nonuniqSections=( ${!'${_inputAssocArray}'[@]} )'

  # sort and dedupe keys, remove TARGET, TARGETFILE, BASEARCH, RELEASEVER
  # from sort input.
  IFS=$'\n'
  _uniqSections=( \
    $(printf "%s\n" "${_nonuniqSections[@]}" \
      | ${g_path_sed} -e 's/^ *//' -e '/^$/d' -e 's/,.*//' \
        -e '/^TARGET$/d' -e '/^TARGETFILE$/d' \
        -e '/^BASEARCH$/d' -e '/^RELEASEVER$/d' \
      | ${g_path_sort} \
      | ${g_path_uniq} )
  )
  IFS="${_defaultIFS}"
  if [[ ${g_debug} -ne 0 ]]; then
    printf "DEBUG (${FUNCNAME}): keys (uniq): "
    printf "%s, " "${_uniqSections[@]}"
    printf "\n"
  fi

  for _section in ${_uniqSections[@]}; do
    eval "$_outputVarName+=( \"${_section}\" )"
  done
}

# loadConfigDefaultsForTarget()
# /*!
# @abstract Load repo defaults for specified target
# @discussion
# Loads the KEY=VAL defaults for the target specified by target and writes an
#   associative array to a global var passed by name in outputVarName.
#   The outputVarName must not be quoted by the caller:
#       loadConfigDefaultsForTarget myOutputVar "target"
#
#   In the above example, the caller can then access values:
#       ${myOutputVar[KEY]}
#
#   The following targets are supported:
#       - CentOS-Base
#       - CentOS-Sources
#       - epel
#
#   NOTE: This function will unset (clear all values in) the associative array
#   pointed to by outputVarName.
# @param outputVarName Name of global variable into which output will be copied.
# @param inputFilePath Name of target.
# @return Returns status 0 on success, status 1 on failure (or 128 if an
#   unsupported target was specified).
# */
loadConfigDefaultsForTarget() {
  local _outputVarName="$1"
  local _target="$2"
  local _repo_id=""
  local _assignOp='='
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}

  if [ -z "${_target}" ]; then
    return 1
  fi

  # reset the incoming array to clear all values, then declare it again
  unset "$_outputVarName"
  declare -g -A "$_outputVarName"

  #
  # REPO defaults
  #

  # CentOS-Base
  local _centos_base_filename="CentOS-Base.repo"
  local _centos_base_name=$(getEscapedStr 'CentOS-$releasever - Base')
  local _centos_base_baseurl=""
  local _centos_base_mirrorlist=$(getEscapedStr 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os')
  local _centos_base_mirrors=""
  local _centos_base_enabled=1
  local _centos_updates_name=$(getEscapedStr 'CentOS-$releasever - Updates')
  local _centos_updates_baseurl=""
  local _centos_updates_mirrorlist=$(getEscapedStr 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=updates')
  local _centos_updates_mirrors=""
  local _centos_updates_enabled=1
  local _centos_extras_name=$(getEscapedStr 'CentOS-$releasever - Extras')
  local _centos_extras_baseurl=""
  local _centos_extras_mirrorlist=$(getEscapedStr 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=extras')
  local _centos_extras_mirrors=""
  local _centos_extras_enabled=1
  local _centos_centosplus_name=$(getEscapedStr 'CentOS-$releasever - Plus')
  local _centos_centosplus_baseurl=""
  local _centos_centosplus_mirrorlist=$(getEscapedStr 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=centosplus')
  local _centos_centosplus_mirrors=""
  local _centos_centosplus_enabled=0
  # CentOS-Sources
  local _centos_sources_filename="CentOS-Sources.repo"
  local _centos_base_source_name=$(getEscapedStr 'CentOS-$releasever - Base Sources')
  local _centos_base_source_baseurl=$(getEscapedStr 'http://vault.centos.org/centos/$releasever/os/Source/')
  local _centos_base_source_mirrorlist=""
  local _centos_base_source_mirrors=""
  local _centos_base_source_enabled=0
  local _centos_updates_source_name=$(getEscapedStr 'CentOS-$releasever - Updates Sources')
  local _centos_updates_source_baseurl=$(getEscapedStr 'http://vault.centos.org/centos/$releasever/updates/Source/')
  local _centos_updates_source_mirrorlist=""
  local _centos_updates_source_mirrors=""
  local _centos_updates_source_enabled=0
  local _centos_extras_source_name=$(getEscapedStr 'CentOS-$releasever - Extras Sources')
  local _centos_extras_source_baseurl=$(getEscapedStr 'http://mirror.centos.org/centos/$releasever/extras/\$basearch/')
  local _centos_extras_source_mirrorlist=""
  local _centos_extras_source_mirrors=""
  local _centos_extras_source_enabled=0
  local _centos_centosplus_source_name=$(getEscapedStr 'CentOS-$releasever - Plus Sources')
  local _centos_centosplus_source_baseurl=$(getEscapedStr 'http://vault.centos.org/centos/$releasever/centosplus/Source/')
  local _centos_centosplus_source_mirrorlist=""
  local _centos_centosplus_source_mirrors=""
  local _centos_centosplus_source_enabled=0
  # epel
  local _epel_epel_filename="epel.repo"
  local _epel_epel_name=$(getEscapedStr 'Extra Packages for Enterprise Linux 7 - $basearch')
  local _epel_epel_baseurl=""
  local _epel_epel_mirrorlist=$(getEscapedStr 'https://mirrors.fedoraproject.org/metalink?repo=epel-$releasever&arch=$basearch')
  local _epel_epel_mirrors=""
  local _epel_epel_enabled=1
  local _epel_epel_debuginfo_name=$(getEscapedStr 'Extra Packages for Enterprise Linux 7 - $basearch - Debug')
  local _epel_epel_debuginfo_baseurl=""
  local _epel_epel_debuginfo_mirrorlist=$(getEscapedStr 'https://mirrors.fedoraproject.org/metalink?repo=epel-debug-$releasever&arch=$basearch')
  local _epel_epel_debuginfo_mirrors=""
  local _epel_epel_debuginfo_enabled=0
  local _epel_epel_source_name=$(getEscapedStr 'Extra Packages for Enterprise Linux 7 - $basearch - Source')
  local _epel_epel_source_baseurl=""
  local _epel_epel_source_mirrorlist=$(getEscapedStr 'https://mirrors.fedoraproject.org/metalink?repo=epel-source-$releasever&arch=$basearch')
  local _epel_epel_source_mirrors=""
  local _epel_epel_source_enabled=0

  _target="${_target,,}" # convert to lowercase
  case "${_target}" in
    centos-base)
      eval "IFS=${_assignOp} $_outputVarName[TARGET]=${_target}"
      eval "IFS=${_assignOp} $_outputVarName[TARGETFILE]=${_centos_base_filename}"
      eval "IFS=${_assignOp} $_outputVarName[BASEARCH]=${g_repo_basearch}"
      eval "IFS=${_assignOp} $_outputVarName[RELEASEVER]=${g_repo_releasever}"
      # [base]
      _repo_id="BASE"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_base_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_base_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_base_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_base_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_base_enabled}"
      # [updates]
      _repo_id="UPDATES"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_updates_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_updates_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_updates_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_updates_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_updates_enabled}"
      # [extras]
      _repo_id="EXTRAS"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_extras_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_extras_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_extras_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_extras_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_extras_enabled}"
      # [centosplus]
      _repo_id="CENTOSPLUS"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_centosplus_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_centosplus_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_centosplus_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_centosplus_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_centosplus_enabled}"
      ;;
    centos-sources)
      eval "IFS=${_assignOp} $_outputVarName[TARGET]=${_target}"
      eval "IFS=${_assignOp} $_outputVarName[TARGETFILE]=${_centos_sources_filename}"
      eval "IFS=${_assignOp} $_outputVarName[BASEARCH]=${g_repo_basearch}"
      eval "IFS=${_assignOp} $_outputVarName[RELEASEVER]=${g_repo_releasever}"
      # [base-source]
      _repo_id="BASE-SOURCE"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_base_source_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_base_source_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_base_source_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_base_source_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_base_source_enabled}"
      # [updates-source]
      _repo_id="UPDATES-SOURCE"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_updates_source_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_updates_source_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_updates_source_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_updates_source_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_updates_source_enabled}"
      # [extras-source]
      _repo_id="EXTRAS-SOURCE"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_extras_source_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_extras_source_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_extras_source_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_extras_source_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_extras_source_enabled}"
      # [centosplus-source]
      _repo_id="CENTOSPLUS-SOURCE"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_centos_centosplus_source_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_centos_centosplus_source_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_centos_centosplus_source_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_centos_centosplus_source_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_centos_centosplus_source_enabled}"
      ;;
    epel)
      eval "IFS=${_assignOp} $_outputVarName[TARGET]=${_target}"
      eval "IFS=${_assignOp} $_outputVarName[TARGETFILE]=${_epel_epel_filename}"
      eval "IFS=${_assignOp} $_outputVarName[BASEARCH]=${g_repo_basearch}"
      eval "IFS=${_assignOp} $_outputVarName[RELEASEVER]=${g_repo_releasever}"
      # [epel]
      _repo_id="EPEL"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_epel_epel_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_epel_epel_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_epel_epel_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_epel_epel_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_epel_epel_enabled}"
      # [epel-debuginfo]
      _repo_id="EPEL-DEBUGINFO"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_epel_epel_debuginfo_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_epel_epel_debuginfo_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_epel_epel_debuginfo_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_epel_epel_debuginfo_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_epel_epel_debuginfo_enabled}"
      # [epel-source]
      _repo_id="EPEL-SOURCE"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},NAME]=${_epel_epel_source_name}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},BASEURL]=${_epel_epel_source_baseurl}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORLIST]=${_epel_epel_source_mirrorlist}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},MIRRORS]=${_epel_epel_source_mirrors}"
      eval "IFS=${_assignOp} $_outputVarName[${_repo_id},ENABLED]=${_epel_epel_source_enabled}"
      ;;
    *)
      return 128
      ;;
  esac

  return 0
}

# checkRepoReachHttp()
# /*!
# @abstract Check if a repo is reachable at a particular IP address
# @discussion
# Attempt to connect to a repo via http. A repoKey can be supplied to provide
#   some level of verifying authenticity; if a repoKey is provided, attempt to
#   retrieve the remote key and compare: non-existing and un-matching keys will
#   result in a reachability failure.
#
#   A sha1 key can be generated using a random string and openssl:
#       >echo -n "value" | openssl sha1 -hmac "key"
#
#   The repoKey can be provided as part of the URL or as an argument. If part of
#   repoURL, the format for the URL is repoKey@repoURL, for example:
#       9502a989997cac7615ffeb10930a73c00d13d2fc@http://10.0.1.1/mirror/centos
#
#   If a repoKey appears in the repoURL and a key is also provided as an
#   argument, the key in the url will take precedence.
#
#   The URL path components will be checked in sequence in search of a REPO-KEY.
#   Searching will continue until a matching key is found, or each of the path
#   components has been examined. Search is conducted in increasing order of
#   depth beginning at the root.
#
# @param repoURL URL of the repo to check.
# @param repoTimeout An integer specifying the number of seconds to wait until
#   abandoning the connection attempt.
# @param repoKey An optional string containing a sha1 key to compare against one
#   retrieved from the remote repo on successful connection.
# @return Returns 1 on success (status 0), 0 on failure (status 1, or 128 if a
#   repoKey was provided and unmatched).
# */
checkRepoReachHttp() {
  local _repoURL=""
  local _repoTimeout=5
  local _repoKey=""
  local _resp _rslt
  local _key _url
  local _curlCmd=""
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}

  if [ -n "${1}" ]; then
    _repoURL="${1}"
    # remove all trailing / and replace with just one (if not a query)
    if [[ -n "${_repoURL##*\?*}" ]]; then
      shopt -s extglob; _repoURL="${_repoURL%%+(/)}/"; shopt -u extglob
    fi
    # add the key@url delimiter if missing to facilitate parsing later
    local _repoRegex2="^[[:alnum:]]+[@]{1}[[:alnum:][:punct:]]+$"
    if [[ ! "${_repoURL}" =~ ${_repoRegex2} ]]; then
      _repoURL="@${_repoURL}"
      # prepend the argument key if one is provided as an argument
      if [[ -n "${3}" ]]; then
        _repoURL="${3}${_repoURL}"
      fi
    fi
  else
    echo 0; return 1
  fi

  if [[ -n "${2}" && $(isNumber "${2}") -eq 1 && ${2} -gt 0 ]]; then
    _repoTimeout=${2}
  fi

  # setup basic curl cmd
  _curlCmd="${g_path_curl} -q --silent --connect-timeout ${_repoTimeout}"

  # fetch header, parse first line for "200 OK"
  #curl -s --dump-header - http://10.0.1.42/mirror/epel/ -o /dev/null
  # with timeout in seconds
  #curl -v -q -s --connect-timeout 5 --dump-header - http://10.0.1.42/mirror/epel/ -o /dev/null
  #
  # NOTE: ALWAYS end url with a '/' for consistent error reporting!
  #
  # Instead of trying to get fancy and parse a dumped header, let's just look
  # at the exit status (there's a whole list if you man curl).
  #
  # curl -q -s --connect-timeout 5 http://8.8.8.8/ -o /dev/null; echo "R: $?"
  #

  # split _repoURL into an array with key and url parts
  IFS='@' _repoArray=(${_repoURL}) IFS=${_defaultIFS}
  _repoKey="${_repoArray[0]}"
  _repoURL="${_repoArray[1]}"

  # check reachability
  _resp=$(${_curlCmd} --fail --output /dev/null "${_repoURL}")
  _rslt=$?
  if [[ ${_rslt} -ne 0 ]]; then
    echo 0; return 1
  fi

  # check for key if requested
  #
  # We don't know beforehand where the REPO-KEY is located, so we check each
  # directory part of the url for a key and stop as soon as we find a match.
  # =>TODO!!!
  #
  if [[ -n "${_repoKey}" ]]; then
    # break apart the url and start searching from host/firstdir (we assume the
    # key won't be located on the host root).
    local _repoBaseURL=""
    local _repoPathURL=""
    local _repoDirString=""
    local _repoDirs=()
    local _repoDirSep='/'
    local _dir=""

    # get url host part
    _repoBaseURL=$(getUrlHost "${_repoURL}")

    # get url path part
    _repoDirString=$(getUrlPath "${_repoURL}")

    # split directory parts into an array
    IFS="${_repoDirSep}" _repoDirs=('' ${_repoDirString}) IFS=${_defaultIFS}
    # _repoDirs=( '' ${_repoDirString//$_repoDirSep/ } )

    # iterate over url path, searching for REPO-KEY, stop on first match
    for _dir in "${_repoDirs[@]}"; do
      _repoPathURL+="${_dir}${_dir:+/}"
      _resp=$(${_curlCmd} --fail "${_repoBaseURL}${_repoPathURL}${g_repokey_name}")
      if [[ "${_repoKey}" == "${_resp}" ]]; then
        echo 1; return 0
      fi
    done

    # no matching key found, return error 128
    echo 0; return 128
  fi

  echo 1; return 0
}

# dumpConfig()
# /*!
# @abstract Dump the global config array
# @discussion
# Iterate over the global config array and print keys and values. This function
#   will only produce output if the "$g_debug" variable has been set so it does
#   not need to be wrapped in a debug conditional. Keys will be alphabetically
#   sorted (in ascending order) before output.
# */
dumpConfig() {
  # get name of calling function
  local _funcname="${FUNCNAME[1]}"
  local _count=0
  local _configKeys=( ${!gConfig[@]} )
  local _configKeysSorted=()
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}

  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG ($_funcname) ==== gConfig"
      # sort keys
      #
      # We will insert TARGET, TARGETFILE, BASEARCH, RELEASEVER manually so we
      # remove them from sort.
      #
    IFS=$'\n'
    _configKeysSorted=( TARGET TARGETFILE BASEARCH RELEASEVER \
      $(printf "%s\n" "${_configKeys[@]}" \
      | ${g_path_sed} -e 's/^ *//' -e '/^$/d' \
        -e '/^TARGET$/d' -e '/^TARGETFILE$/d' \
        -e '/^BASEARCH$/d' -e '/^RELEASEVER$/d' \
      | ${g_path_sort} -df) )
    IFS="${_defaultIFS}"
    for sorted_key in ${_configKeysSorted[@]}; do
      (( _count++ ))
      printf "DEBUG (${_funcname}): %02d [${sorted_key}]: ${gConfig[$sorted_key]}\n" "${_count}"
    done
  fi
}

# showSettings()
# /*!
# @abstract Show all internal settings to be applied
# @discussion
# Iterate over the global config array and print keys and value. Output any
#   other internal values that will affect results. Keys will be alphabetically
#   sorted (in ascending order) before output.
# */
showSettings() {
  local _count=0
  local _configKeys=( ${!gConfig[@]} )
  local _configKeysSorted=()
  local IFS=$' \t\n'
  local _defaultIFS=${IFS}

  # sort keys
  if [[ ${g_debug} -ne 0 ]]; then
    printf "DEBUG (${FUNCNAME}): keys (orig): "
    printf "%s, " "${_configKeys[@]}"
    printf "\n"
  fi
  # sort keys
  #
  # We will insert TARGET, TARGETFILE, BASEARCH, RELEASEVER manually so we
  # remove them from sort.
  #
  IFS=$'\n'
  _configKeysSorted=( TARGET TARGETFILE BASEARCH RELEASEVER \
    $(printf "%s\n" "${_configKeys[@]}" \
    | ${g_path_sed} -e 's/^ *//' -e '/^$/d' \
      -e '/^TARGET$/d' -e '/^TARGETFILE$/d' \
      -e '/^BASEARCH$/d' -e '/^RELEASEVER$/d' \
    | ${g_path_sort} -df) )
  IFS="${_defaultIFS}"
  if [[ ${g_debug} -ne 0 ]]; then
    printf "DEBUG (${FUNCNAME}): keys (sort): "
    printf "%s, " "${_configKeysSorted[@]}"
    printf "\n"
  fi

  echo "==== Merged settings"
  for sorted_key in ${_configKeysSorted[@]}; do
    (( _count++ ))
    printf "%02d ${sorted_key}: ${gConfig[$sorted_key]}\n" "${_count}"
  done
}

#
# main script
#
g_runos=$(getRunOS)
g_path_script=$(getRealPath "${BASH_SOURCE[0]}")
g_path_scriptdir=$(getDirName "${g_path_script}")
g_path_templates="${g_path_scriptdir}/templates"
g_path_infile="${g_path_templates}/CentOS-Base.repo.tmpl"

# init our config associative array with defaults
declare -A gConfig
loadConfigDefaultsForTarget gConfig "${g_repo_target}"

# parse cli parameters
#
# Our options:
#   --use-config, -c
#   --in-file=path, -i
#   --out-file=path, -o
#   --out-stdout, -O
#   --repo-baseurl=URL, -b
#   --repo-mirrorlist=URL, -m
#   --repo-id=name, -n
#   --repo-name=name, -l
#   --private-mirrors=list, -p
#   --target=name, -t
#   --update-yum, -u
#   --repo-disable, -x
#   --repo-enabled, -y
#   --repo-basearch, -a
#   --repo-releasever, -r
#   --repo-staticvars, -s
#   --debug, d
#   --force, f
#   --help, h
#   --version, v
#   --show-settings, z
#
_params=""
${g_path_getopt} -T > /dev/null
if [ $? -eq 4 ]; then
  # GNU enhanced getopt is available
  _long_opts=" \
    use-config,\
    in-file:,\
    out-file:,\
    out-stdout:,\
    repo-baseurl:,\
    repo-mirrorlist:,\
    repo-id:,\
    repo-name:,\
    private-mirrors:,\
    target:,\
    update-yum:,\
    repo-disable,\
    repo-enable,\
    repo-basearch,\
    repo-releasever,\
    repo-staticvars,\
    new-repokey,\
    force,\
    help,\
    version,\
    debug,\
    show-settings\
  "
  _params="$(${g_path_getopt} --name "${g_progname}" --long "${_long_opts}" --options ci:o:b:m:n:l:p:t:uxya:r:skfhvdz -- "$@")"
  unset _long_opts
else
  # Original getopt is available
  g_getopt_old=1
  _params="$(${g_path_getopt} ci:o:b:m:n:l:p:t:uxya:r:skfhvdz "$@")"
fi

# check for invalid params passed; bail out if error is set.
if [ $? -ne 0 ]
then
  usage; exit 1;
fi

eval set -- "$_params"
unset _params
_params_count=$#

while [ $# -gt 0 ]; do
  case "$1" in
    -c | --use-config)
      cli_USE_CONFIG=1; g_use_config=${cli_USE_CONFIG};
      ;;
    -i | --in-file)
      cli_INFILE="$2";
      shift;
      ;;
    -o | --out-file)
      if [[ ${g_update_yum} -ne 0 ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo "-o option specified simultaneously with -u option";
        fi
        usage;
        exit 1;
      fi
      cli_OUTFILE="$2";
      shift;
      ;;
    -b | --repo-baseurl)
      cli_REPO_BASEURL="$2";
      shift;
      ;;
    -m | --repo-mirrorlist)
      cli_REPO_MIRRORLIST="$2";
      shift;
      ;;
    -n | --repo-id)
      _param="${2,,}" # convert to lowercase
      case "${_param}" in
        base) ;&
        updates) ;&
        extras) ;&
        centosplus) ;&
        base-source) ;&
        updates-source) ;&
        extras-source) ;&
        centosplus-source) ;&
        epel) ;&
        epel-debuginfo) ;&
        epel-source)
          cli_REPO_ID="$2"; g_repo_id=${_param};
          shift
          ;;
        *)
          echo "-n option value is invalid";
          usage;
          exit 1;
          ;;
      esac
      unset _param
      ;;
    -l | --repo-name)
      cli_REPO_NAME="$2";
      shift;
      ;;
    -p | --private-mirrors)
      cli_PRIVATE_MIRRORS="$2";
      shift;
      ;;
    -t | --target)
      _param="${2,,}" # convert to lowercase
      case "${_param}" in
        centos-base) ;&
        centos-sources) ;&
        epel)
          cli_REPO_TARGET="$2"; g_repo_target_cli=${_param};
          shift
          ;;
        *)
          echo "-t option value is invalid";
          usage;
          exit 1;
          ;;
      esac
      unset _param
      ;;
    -u | --update-yum)
      if [[ -n "${cli_OUTFILE}" ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo "-u option specified simultaneously with -o option";
        fi
        usage;
        exit 1;
      fi
      cli_UPDATE_YUM=1; g_update_yum=${cli_UPDATE_YUM};
      ;;
    -x | --repo-disable)
      if [[ -n "${cli_REPO_ENABLE}" ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo "-y option specified simultaneously with -x option";
        fi
        usage;
        exit 1;
      fi
      cli_REPO_DISABLE=1; g_repo_enable=0;
      ;;
    -y | --repo-enable)
      if [[ -n "${cli_REPO_DISABLE}" ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo "-x option specified simultaneously with -y option";
        fi
        usage;
        exit 1;
      fi
      cli_REPO_ENABLE=1; g_repo_enable=1;
      ;;
    -a | --repo-basearch)
      _param="${2,,}" # convert to lowercase
      case "${_param}" in
        i386) ;&
        x86_64)
          cli_REPO_BASEARCH="$2"; g_repo_basearch_cli=${_param};
          shift
          ;;
        *)
          echo "-a option value is invalid";
          usage;
          exit 1;
          ;;
      esac
      unset _param
      ;;
    -r | --repo-releasever)
      _param="${2,,}" # convert to lowercase
      if [[ -n "${_param}" && $(isNumber "${_param}") -eq 1 && ${_param/.*} -gt 0 ]]; then
        cli_REPO_RELEASEVER="$2"; g_repo_releasever_cli=${_param};
        shift
      else
        echo "-r option value is invalid";
        usage;
        exit 1;
      fi
      unset _param
      ;;
    -s | --repo-staticvars)
      cli_REPO_STATICVARS=1; g_repo_staticvars=${cli_REPO_STATICVARS};
      ;;
    -k | --new-repokey)
      if [[ ( ${_params_count} -eq 3 && ${g_debug} -eq 0 ) || ( ${_params_count} -gt 3 ) ]]; then
        if [ "${g_debug}" -ne 0 ]; then
          echo "-k option with additional options";
        fi
        usage;
        exit 1;
      fi
      getRepoKey
      exit $?
      ;;
    -d | --debug)
      if [[ ${_params_count} -eq 2 ]]; then
        echo "-d option without additional options"
        usage;
        exit 1;
      fi
      cli_DEBUG=1; g_debug=${cli_DEBUG};
      ;;
    -f | --force)
      cli_FORCEEXEC=1; g_forceexec=${cli_FORCEEXEC};
      if [[ ${g_debug} -ne 0 ]]; then
        echo "-f option not implemented";
      fi
      ;;
    -v | --version)
      if [[ ( ${_params_count} -eq 3 && ${g_debug} -eq 0 ) || ( ${_params_count} -gt 3 ) ]]; then
        if [ "${g_debug}" -ne 0 ]; then
          echo "-v option with additional options";
        fi
        usage;
        exit 1;
      fi
      version
      exit 0
      ;;
    -h | --help)
      if [[ ( ${_params_count} -eq 3 && ${g_debug} -eq 0 ) || ( ${_params_count} -gt 3 ) ]]; then
        if [ "${g_debug}" -ne 0 ]; then
          echo "-h option with additional options";
        fi
        usage;
        exit 1;
      fi
      help
      exit 0
      ;;
    -z | --show-settings)
       cli_SHOWVARS=1; g_show_vars=${cli_SHOWVARS};
       ;;
    --)
      shift;
      break;
      ;;
  esac
  shift
done

# runos?
if [[ ${g_debug} -ne 0 ]]; then
  printf "DEBUG:         RunOS: %s\n" "${g_runos}"
  printf "DEBUG:   Script Path: %s\n" "${g_path_script}"
  printf "DEBUG:    Script Dir: %s\n" "${g_path_scriptdir}"
  printf "DEBUG: Templates Dir: %s\n" "${g_path_templates}"
  printf "DEBUG:  Def Template: %s\n" "${g_path_infile}"
fi

# bail out if user hasn't specified any options
if [[ ${_params_count} -eq 1 ]]; then
  echo
  echo "ABORTING. You have not specified any actionable options."
  echo
  usage;
  exit 1;
fi

argavail=$#
argparse=0
arglines_array=()

# parse args or parse redirected input lines (read stdin if no args)
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: cliargs: ${argavail}"
fi

# max args we accept is 1: an ini file for input
if [[ ${g_use_config} -eq 0 &&  ${argavail} -gt 0 ]] \
  || [[ ${g_use_config} -eq 1 && ${argavail} -gt 1 ]]; then
  echo
  echo "ABORTING. Too many additional arguments specified."
  echo
  usage
  exit 1
fi

# Parse INI file (if provided)
#
if [[ ${g_use_config} -eq 1 ]]; then
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Updating config with ini file parameters."
  fi
  _lineRegex1="^[[:blank:]#;]+.*$"
  _lineRegex2="^[a-zA-Z_\-]+([=]{0}|[=]{2,})[a-zA-Z0-9\_\!\"\.\/\-]*$"

  if [[ ${argavail} -gt 0 ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Parsing ini data via direct file access."
    fi

    # args available; parse them
    while [[ ${argavail} -gt 0 ]]; do
      (( argparse++ ))
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${1}"
      fi
      arglines_array+=("${1}")
      shift
      argavail=$#
    done

    # grab iniPath and reset arglines_array for reuse
    _iniPath="${arglines_array[0]}"
    arglines_array=()
    argparse=0

    # check file access
    if [[ -f "${_iniPath}" ]] && [[ ! -r "${_iniPath}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: ABORTING. Unable to access config file: ${_iniPath}"
        echo "DEBUG: Must be a read permissions error."
      fi
      exit 1
    elif [[ ! -f "${_iniPath}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: ABORTING. Unable to access config file: ${_iniPath}"
        echo "DEBUG: The config file appears to be missing."
      fi
      exit 1
    fi

    # read file and stuff each line in arglines_array
    while IFS= read -r line; do
      # skip:
      #  - empty lines,
      #  - lines starting with a space, hash, semicolon
      if [[ -z "${line}" || "${line}" =~ ${_lineRegex1} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (blank or comment, skipping)\n" "${line}"
        fi
        continue;
      fi
      # skip:
      #  - lines that don't contain exactly one '='
      #
      # NOTE: This is a syntax error in the ini file.
      #
      #
      if [[ "${line}" =~ ${_lineRegex2} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (malformed, skipping)\n" "${line}"
        fi
        continue;
      fi
      (( argparse++ ))
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${line}"
      fi
      arglines_array+=("${line}")
    done <<< "$(${g_path_cat} ${_iniPath})"

    unset _iniPath
  else
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Parsing ini data via input redirection."
    fi

    # no args, must be stdin; read lines
    while IFS= read -r line; do
      # skip:
      #  - empty lines,
      #  - lines starting with a space, hash, semicolon
      if [[ -z "${line}" || "${line}" =~ ${_lineRegex1} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (blank or comment, skipping)\n" "${line}"
        fi
        continue;
      fi
      # skip:
      #  - lines that don't contain exactly one '='
      #
      # NOTE: This is a syntax error in the ini file.
      #
      #
      if [[ "${line}" =~ ${_lineRegex2} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (malformed, skipping)\n" "${line}"
        fi
        continue;
      fi
      (( argparse++ ))
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${line}"
      fi
      arglines_array+=("${line}")
    done
    # close stdin (the redirected file, we've read it already)
    exec <&-
    # open /dev/tty as stdin so we can read user prompts if needed
    exec 0</dev/tty
  fi

  unset _lineRegex1
  unset _lineRegex2
fi

# Rangle our vars
#

# load defaults based on target
#
# We need to extract the target from the INI file if provided.
#
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Updating target from INI and cli."
fi

# extract target from INI
#
# The easiest way to get the INI TARGET value is to just parse the whole thing.
#
if [[ ${g_use_config} -eq 1 ]]; then
  declare -A _configTmp
  INIDM=',' readConfigFromArray _configTmp arglines_array[@]

  # remove the .repo extension from TARGET (we allow it in the ini file)
  _configTmp[TARGET]="${_configTmp[TARGET]%.repo}"

  if [[ -n "${_configTmp[TARGET]}" ]]; then
    _target=${_configTmp[TARGET]}
    _target="${_target,,}" # convert to lowercase
    case "${_target}" in
      centos-base) ;&
      centos-sources) ;&
      epel)
        # warn in debug if target is set to different vals on cli and in INI
        if [[ -n "${g_repo_target_cli}" && ( "${g_repo_target_cli}" != "${_target}" ) ]]; then
          if [[ ${g_debug} -ne 0 ]]; then
            echo -ne "\033[1m\E[31m"
            echo "DEBUG: WARNING: Target mismatch on CLI and in config file!"
            echo
            echo "DEBUG:          config target: ${_target}"
            echo "DEBUG:             cli target: ${g_repo_target_cli}"
            echo
            echo "DEBUG: WARNING: Value from cli will override config setting."
            echo -ne "\E[0m\033[0m"
            if [[ "${g_forceexec}" -eq 0 ]]; then
              # prompt user for confirmation
              if [[ "no" == $(promptConfirm "Continue anyway?") ]]
              then
                echo "ABORTING. Nothing to do."
                exit 0
              fi
            fi
            echo -ne "\033[1m\E[31m"
            echo "DEBUG: Continuing."
            echo -ne "\E[0m\033[0m"
          fi
        fi
        g_repo_target_ini="${_target}"
        ;;
      *)
        echo
        echo "ABORTING. Invalid target specified in config file: ${_configTmp[TARGET]}."
        echo
        exit 1;
        ;;
    esac
    unset _target
  fi

  # validate the BASEARCH parameter and warn if cli and ini are different
  if [[ -n "${_configTmp[BASEARCH]}" ]]; then
    _basearch=${_configTmp[BASEARCH]}
    _basearch="${_basearch,,}" # convert to lowercase
    case "${_basearch}" in
      i386) ;&
      x86_64)
        # warn in debug if basearch is set to different vals on cli and in INI
        if [[ -n "${g_repo_basearch_cli}" && ( "${g_repo_basearch_cli}" != "${_basearch}" ) ]]; then
          if [[ ${g_debug} -ne 0 ]]; then
            echo -ne "\033[1m\E[31m"
            echo "DEBUG: WARNING: Basearch mismatch on CLI and in config file!"
            echo
            echo "DEBUG:        config basearch: ${_basearch}"
            echo "DEBUG:           cli basearch: ${g_repo_basearch_cli}"
            echo
            echo "DEBUG: WARNING: Value from cli will override config setting."
            echo -ne "\E[0m\033[0m"
            if [[ "${g_forceexec}" -eq 0 ]]; then
              # prompt user for confirmation
              if [[ "no" == $(promptConfirm "Continue anyway?") ]]
              then
                echo "ABORTING. Nothing to do."
                exit 0
              fi
            fi
            echo -ne "\033[1m\E[31m"
            echo "DEBUG: Continuing."
            echo -ne "\E[0m\033[0m"
          fi
        fi
        g_repo_basearch_ini="${_basearch}"
        ;;
      *)
        echo
        echo "ABORTING. Invalid basearch specified in config file: ${_configTmp[BASEARCH]}."
        echo
        exit 1;
        ;;
    esac
    unset _basearch
  fi

  # validate the RELEASEVER parameter and warn if cli and ini are different
  if [[ -n "${_configTmp[RELEASEVER]}" ]]; then
    _releasever=${_configTmp[RELEASEVER]}
    _releasever="${_releasever,,}" # convert to lowercase
    if [[ -n "${_releasever}" && $(isNumber "${_releasever}") -eq 1 && ${_releasever/.*} -gt 0 ]]; then
      # warn in debug if basearch is set to different vals on cli and in INI
      if [[ -n "${g_repo_releasever_cli}" && ( "${g_repo_releasever_cli}" != "${_releasever}" ) ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo -ne "\033[1m\E[31m"
          echo "DEBUG: WARNING: ReleaseVer mismatch on CLI and in config file!"
          echo
          echo "DEBUG:      config releasever: ${_releasever}"
          echo "DEBUG:         cli releasever: ${g_repo_releasever_cli}"
          echo
          echo "DEBUG: WARNING: Value from cli will override config setting."
          echo -ne "\E[0m\033[0m"
          if [[ "${g_forceexec}" -eq 0 ]]; then
            # prompt user for confirmation
            if [[ "no" == $(promptConfirm "Continue anyway?") ]]
            then
              echo "ABORTING. Nothing to do."
              exit 0
            fi
          fi
          echo -ne "\033[1m\E[31m"
          echo "DEBUG: Continuing."
          echo -ne "\E[0m\033[0m"
        fi
      fi
      g_repo_releasever_ini="${_releasever}"
    else
      echo
      echo "ABORTING. Invalid releasever specified in config file: ${_configTmp[RELEASEVER]}."
      echo
      exit 1;
    fi
    unset _releasever
  fi

  unset _configTmp
fi

# update target from cli or ini (in that order)
if [[ -n "${g_repo_target_cli}" ]]; then
  g_repo_target="${g_repo_target_cli}"
elif [[ -n "${g_repo_target_ini}" ]]; then
  g_repo_target="${g_repo_target_ini}"
fi

# update basearch from cli or ini (in that order)
if [[ -n "${g_repo_basearch_cli}" ]]; then
  g_repo_basearch="${g_repo_basearch_cli}"
elif [[ -n "${g_repo_basearch_ini}" ]]; then
  g_repo_basearch="${g_repo_basearch_ini}"
fi

# update releasever from cli or ini (in that order)
if [[ -n "${g_repo_releasever_cli}" ]]; then
  g_repo_releasever="${g_repo_releasever_cli}"
elif [[ -n "${g_repo_releasever_ini}" ]]; then
  g_repo_releasever="${g_repo_releasever_ini}"
fi

# reload defaults for target if different
if [[ "${g_repo_target}" != "${gConfig[TARGET]}" ]]; then
  loadConfigDefaultsForTarget gConfig "${g_repo_target}"
fi

#
# merge INI config parameters (ignore target if we have it on cli)
#
if [[ ${g_use_config} -eq 1 ]]; then
  dumpConfig
  INIDM=',' readConfigFromArray gConfig arglines_array[@]

  # remove the .repo extension from TARGET (we allow it in the ini file)
  gConfig[TARGET]="${gConfig[TARGET]%.repo}"

  # overwrite TARGET from g_repo_target
  #
  # If we have a cli target and an ini target, the target will now override the
  # cli value just-loaded with defaults; we need to restore it.
  if [[ -n "${g_repo_target_ini}" ]]; then
    gConfig[TARGET]="${g_repo_target}"
  fi

  # overwrite BASEARCH from g_repo_basearch
  if [[ -n "${g_repo_basearch_ini}" ]]; then
    gConfig[BASEARCH]="${g_repo_basearch}"
  fi

  # overwrite RELEASEVER from g_repo_releasever
  if [[ -n "${g_repo_releasever_ini}" ]]; then
    gConfig[RELEASEVER]="${g_repo_releasever}"
  fi

  dumpConfig
fi

#
# merge CLI config parameters
#
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Updating config with cli parameters."
fi

if [ -n "${cli_REPO_NAME}" ]; then
  gConfig[${g_repo_id^^},NAME]="${cli_REPO_NAME}"
  g_repo_name="${cli_REPO_NAME}"
fi

if [ -n "${cli_REPO_BASEURL}" ]; then
  gConfig[${g_repo_id^^},BASEURL]="${cli_REPO_BASEURL}"
  g_repo_baseurl="${cli_REPO_BASEURL}"
fi

if [ -n "${cli_REPO_MIRRORLIST}" ]; then
  gConfig[${g_repo_id^^},MIRRORLIST]="${cli_REPO_MIRRORLIST}"
  g_repo_mirrorlist="${cli_REPO_MIRRORLIST}"
fi

if [ -n "${cli_PRIVATE_MIRRORS}" ]; then
  gConfig[${g_repo_id^^},MIRRORS]="${cli_PRIVATE_MIRRORS}"
  g_repo_mirrors="${cli_PRIVATE_MIRRORS}"
fi

if [ ${g_repo_enable} -eq 1 ]; then
  gConfig[${g_repo_id^^},ENABLED]="1"
elif [ ${g_repo_enable} -eq 0 ]; then
  gConfig[${g_repo_id^^},ENABLED]="0"
fi

dumpConfig

#
# replace YUM vars with static values
#
if [[ ${g_repo_staticvars} -eq 1 ]]; then
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Updating config for mirror static values."
  fi

  # get a list of sections
  declare -a _configSections
  parseConfigSections _configSections[@] gConfig[@]

  # iterate over sections
  for _section in ${_configSections[@]}; do
    if [[ -n ${gConfig[${_section},NAME]} ]]; then
      gConfig[${_section},NAME]=$(getUrlStaticPath "${gConfig[${_section},NAME]}")
    fi
    if [[ -n ${gConfig[${_section},BASEURL]} ]]; then
      gConfig[${_section},BASEURL]=$(getUrlStaticPath "${gConfig[${_section},BASEURL]}")
    fi
    if [[ -n ${gConfig[${_section},MIRRORLIST]} ]]; then
      gConfig[${_section},MIRRORLIST]=$(getUrlStaticPath "${gConfig[${_section},MIRRORLIST]}")
    fi
    if [[ -n ${gConfig[${_section},MIRRORS]} ]]; then
      gConfig[${_section},MIRRORS]=$(getUrlStaticPath "${gConfig[${_section},MIRRORS]}")
    fi
  done
  unset _section
  unset _configSections

  dumpConfig
fi

#
# determine reachability of mirrors and update baseurl, mirrorlist
#
# If a mirror is provided and reachabile, we update the baseurl for the mirror
# and remove the mirrorlist.
#
# Strategy:
# - If a mirror is provided, and reachable, we update baseurl (and remove the
#   mirrorlist entry)
# - If no mirror, or mirror unreachable and a baseurl is provided, we use it
#   (and remove the mirrorlist entry)
# - Final fallback is to mirrorlist.
#
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Updating config for mirror reachability."
fi

# get a list of sections
declare -a _configSections
parseConfigSections _configSections[@] gConfig[@]

# iterate over sections
for _section in ${_configSections[@]}; do
  _section_lc="${_section,,}"

  # if baseurl is set on cli AND mirrors is unset, clear mirrorlist, baseurl
  # takes precedence over any mirrorlist setting (if present)
  #
  # Normally, baseurl via cli will take precedence over any other setting (set
  # via cli, ini, or default). When specified on cli along with the mirrors
  # option, baseurl will become a fallback if no mirrors are reachable.
  #
  if [[ "${_section_lc}" = "${g_repo_id}" \
    && ( -n "${g_repo_baseurl}" && -z "${g_repo_mirrors}" ) ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      printf "DEBUG: BASEURL is set from cli parameters for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
      printf "       Clearing MIRRORLIST and ignoring mirrors.\n"
    fi
    # remove mirrorlist
    gConfig[${_section},MIRRORLIST]=""
    continue
  fi

  # if mirrorlist is set on cli AND mirrors is unset, clear baseurl, setting
  # mirrorlist from cli takes precedence over default or ini baseurl.
  #
  # Normally, mirrorlist via cli will take precedence over any other setting
  # (set via cli, ini, or default) EXCEPT for baseurl set via cli. When
  # specified on cli along with the mirrors option, mirrorlist will become a
  # fallback if no mirrors are reachable.
  #
  #
  if [[ "${_section_lc}" = "${g_repo_id}" \
    && ( -n "${g_repo_mirrorlist}" && -z "${g_repo_mirrors}" ) ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      printf "DEBUG: MIRRORLIST is set from cli parameters for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
      printf "       Clearing BASEURL and ignoriing MIRRORS.\n"
    fi
    # remove baseurl
    gConfig[${_section},BASEURL]=""
    continue
  fi

  IFS=' ' _mirrorArray=( ${gConfig[${_section},MIRRORS]//,/ } ) IFS="${g_defaultIFS}"
  _mirrorArrayLen="${#_mirrorArray[@]}"
  if [[ ${g_debug} -ne 0 && ${_mirrorArrayLen} -gt 0 ]]; then
    printf "DEBUG: Found %d mirrors for %s section in %s repo.\n" "${_mirrorArrayLen}" "${_section}" "${g_repo_target}"
  fi
  # check reachability of each mirror, select the first one that passes and
  # assign it to baseurl, then flush mirrorlist.
  _mirrorsReached=0
  for _mirror in ${_mirrorArray[@]}; do
    _mirrorCheckUrl="${_mirror}"
    # we can only check static urls (not containing YUM variables) so we have
    # to convert the url first if it has not been converted yet (if static vars
    # option is enabled the url will have been converted already).
    if [[ ${g_repo_staticvars} -ne 1 ]]; then
      _mirrorCheckUrl=$(getUrlStaticPath ${_mirror})
    fi

    if [[ ${g_debug} -ne 0 ]]; then
      echo -n "DEBUG: Checking repo mirror: ${_mirrorCheckUrl} "
    fi

    if [[ $(checkRepoReachHttp ${_mirrorCheckUrl}) -eq 1 ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "[PASS]"
        printf "DEBUG: Reachable repo mirror found for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
        printf "       Updating BASEURL and clearing MIRRORLIST.\n"
      fi
      # strip key from mirror (if present)
      _mirrorURL="${_mirror#*@}"

      # update baseurl, remove mirrorlist, break
      gConfig[${_section},BASEURL]="${_mirrorURL}"
      gConfig[${_section},MIRRORLIST]=""

      (( _mirrorsReached++ ))
      unset _mirrorURL
      break
    else
      if [[ ${g_debug} -ne 0 ]]; then
        echo "[FAIL]"
      fi
    fi
    unset _mirrorCheckUrl
  done

  if [[ ${_mirrorsReached} -gt 0 ]]; then
    continue
  fi

  if [[ ${_mirrorArrayLen} -gt 0 && ${_mirrorsReached} -eq 0 ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      printf "DEBUG: No mirrors reachable for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
    fi
    # fallback to cli provided baseurl
    if [[ "${_section_lc}" = "${g_repo_id}" && -n "${g_repo_baseurl}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: BASEURL is set from cli parameters for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
        printf "       Applying BASEURL set from cli, clearing MIRRORLIST.\n"
      fi
      # remove mirrorlist
      gConfig[${_section},MIRRORLIST]=""
      continue
    fi
    # fallback to cli provided mirrorlist
    if [[ "${_section_lc}" = "${g_repo_id}" && -n "${g_repo_mirrorlist}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: MIRRORLIST is set from cli parameters for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
        printf "       Applying MIRRORLIST set from cli, clearing BASEURL.\n"
      fi
      # remove baseurl
      gConfig[${_section},BASEURL]=""
      continue
    fi
  fi

  #
  # fall back to INI settings or defaults
  #
  if [[ ${g_debug} -ne 0 ]]; then
    printf "DEBUG: Falling back to BASEURL and MIRRORLIST settings from ini and\n"
    printf "       defaults for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
  fi

  # if we have a baseurl, clear mirrorlist
  if [[ -n "${gConfig[${_section},BASEURL]}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      printf "DEBUG: Found BASEURL for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
      printf "       Applying BASEURL. Clearing MIRRORLIST (if set).\n"
    fi
    # remove mirrorlist
    gConfig[${_section},MIRRORLIST]=""
    continue
  fi

  if [[ -n "${gConfig[${_section},MIRRORLIST]}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      printf "DEBUG: Found MIRRORLIST setting for %s section in %s repo.\n" "${_section}" "${g_repo_target}"
      printf "       No BASEURL found. Applying MIRRORLIST.\n"
    fi
    # remove baseurl
    gConfig[${_section},BASEURL]=""
    continue
  fi
done
unset _mirror
unset _mirrorArrayLen
unset _mirrorArray
unset _mirrorsReached
unset _section_lc
unset _section
unset _configSections

dumpConfig

# show settings if -z option is on
if [[ ${g_show_vars} -ne 0 ]]; then
  showSettings

  exit 0
fi

# check in-file and out-file access
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Checking in-file and out-file."
fi

if [ -n "${cli_INFILE}" ]; then
  g_path_infile="${cli_INFILE}";

  if [[ -f "${g_path_infile}" ]] && [[ ! -r "${g_path_infile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access input file: ${g_path_infile}"
      echo "DEBUG: Must be a read permissions error."
    fi
    exit 1
  elif [[ ! -f "${g_path_infile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access input file: ${g_path_infile}"
      echo "DEBUG: The input file appears to be missing."
    fi
    exit 1
  fi
else
  # use default input template
  _templPathBaseDir="${g_path_templates}"
  shopt -s extglob; _templPathBaseDir="${_templPathBaseDir%%+(/)}"; shopt -u extglob
  g_path_infile="${_templPathBaseDir}/${gConfig[TARGETFILE]}.tmpl"
  unset _templPathBaseDir
fi

if [[ -n "${cli_OUTFILE}" || ${g_update_yum} -eq 1 ]]; then
  if [[ ${g_update_yum} -eq 1 ]]; then
    #
    # force stdout on unsupported OS if the -u option was set on cli
    #
    if [[ "${g_runos}" =~ "CentOS" ]]; then
      # CentOS default output paths
      _outPathBaseDir="${g_path_yumconfig}"
      shopt -s extglob; _outPathBaseDir="${_outPathBaseDir%%+(/)}"; shopt -u extglob
      g_path_outfile="${_outPathBaseDir}/${gConfig[TARGETFILE]}"
    elif [[ "${g_runos}" =~ "Fedora" || "${g_runos}" =~ "Fedora" ]]; then
      # Fedora and Redhat use different repo files, we need to support those...
      #
      # Fedora:
      #   /etc/yum.repos.d/fedora.repo
      #   /etc/yum.repos.d/fedora-updates.repo
      #
      # Redhat:
      #   /etc/yum.repos.d/rhel-source.repo
      #
      if [[ ${g_debug} -ne 0 ]]; then
        _runOSName=$(getRunOSName "${g_runos}")
        printf "DEBUG: Unsupported OS (%s) forcing output to stdout\n" _runOSName
        unset _runOSName
      fi
      g_use_stdout=1
      g_path_outfile=""
    else
      # unsupported OS, turn on stdout
      if [[ ${g_debug} -ne 0 ]]; then
        _runOSName=$(getRunOSName "${g_runos}")
        printf "DEBUG: Unsupported OS (%s) forcing output to stdout\n" _runOSName
        unset _runOSName
      fi
      g_use_stdout=1
      g_path_outfile=""
    fi
  else
    g_path_outfile="${cli_OUTFILE}";
  fi

  if [[ -n "${g_path_outfile}" ]]; then
    if [[ -f "${g_path_outfile}" ]] && [[ ! -w "${g_path_outfile}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: ABORTING. Unable to access output file: ${g_path_outfile}"
        echo "DEBUG: Must be a write permissions error."
      fi
      exit 1
    elif [[ -f "${g_path_outfile}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: Truncating output file at path: ${g_path_outfile}"
      fi

      _resp=$({ echo > "${g_path_outfile}"; } 2>&1 )
      _rslt=$?
      if [[ ${_rslt} -ne 0 ]]; then
        echo "ABORTING. Unable to truncate output file: ${g_path_outfile}"
        echo "Not sure what the problem is."
        exit 1
      fi
    elif [[ ! -f "${g_path_outfile}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: Unable to access output file: ${g_path_outfile}"
        echo "DEBUG: The output file appears to be missing."
      fi

      # create output file
      _outfiledir=$(${g_path_dirname} "${g_path_outfile}")
      if [[ -d "${_outfiledir}" ]] && [[ $(isPathWriteable "${_outfiledir}") -ne 1 ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo "DEBUG: Unable to access output file directory: ${_outfiledir}"
          echo "DEBUG: Must be a write permissions error."
        fi
        exit 1
      elif [[ ! -d "${_outfiledir}" ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          echo "DEBUG: Unable to access output file directory: ${_outfiledir}"
          echo "DEBUG: The output file directory appears to be missing."
        fi
        exit 1
      fi

      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: Creating output file at path: ${g_path_outfile}"
      fi

      _resp=$({ ${g_path_touch} "${g_path_outfile}"; } 2>&1 )
      _rslt=$?
      if [[ ${_rslt} -ne 0 ]]; then
        echo "ABORTING. Unable to create output file: ${g_path_outfile}"
        echo "Not sure what the problem is."
        exit 1
      fi
      unset _rslt
      unset _resp
      unset _outfiledir
    fi
  fi
fi

# parse the infile and replace tokens
_tmpConfigFile=$(TMPMK=1 mkTempFile "${g_path_worktemp}")
if [[ -z "${_tmpConfigFile}" || $? -ne 0 ]]; then
  echo "ABORTING. Unable to create temporary working file in: ${g_path_worktemp}"
  echo "Not sure what the problem is."
  exit 1
fi
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Created temporary working file: ${_tmpConfigFile}"
fi
${g_path_cat} "${g_path_infile}" >> ${_tmpConfigFile}

# update header
IFS=$'\n' read -r -d '' _repo100 <<'EOF'
This file was auto-generated with repo-config.\
# https://bitbucket.org/tapatip/repo-config
EOF

${g_path_sed} -i.bak "s|%%REPO100%%|${_repo100}|g" "${_tmpConfigFile}"

# iterate through sections and update tokens in file
#
# There are three tokens we need to update for each section:
#   - baseurl
#   - mirrorlist
#   - enabled
#
# If baseurl is set, we need to delete the mirrorlist setting; if mirrorlist
# is set, we need to delete the baseurl setting.
#
declare -a _configSections
parseConfigSections _configSections[@] gConfig[@]

# iterate over sections
for _section in ${_configSections[@]}; do
  # name
  if [[ -n "${gConfig[${_section},NAME]}" ]]; then
    # rewrite url to make sure we have a protocol
    _name="${gConfig[${_section},NAME]}"
    ${g_path_sed} -i.bak "s|%%${_section}_NAME%%|name=${_name}|g" "${_tmpConfigFile}"
    unset _name
  else
    # name is not set, set it to section name
    ${g_path_sed} -i.bak "s|%%${_section}_NAME%%|name=${_section}|g" "${_tmpConfigFile}"
  fi

  # baseurl
  if [[ -n "${gConfig[${_section},BASEURL]}" ]]; then
    # rewrite url to make sure we have a protocol
    _host=$(getUrlHost ${gConfig[${_section},BASEURL]})
    _path=$(getUrlPath ${gConfig[${_section},BASEURL]})
    printf -v _urlWithProto "%s%s" "${_host}" "${_path#/}"
    _urlWithProto="$(getEscapedStr ${_urlWithProto})"
    ${g_path_sed} -i.bak "s|%%${_section}_BASEURL%%|baseurl=${_urlWithProto}|g" "${_tmpConfigFile}"
    unset _urlWithProto
    unset _path
    unset _host
  else
    ${g_path_sed} -i.bak "/%%${_section}_BASEURL%%/d" "${_tmpConfigFile}"
  fi

  # mirrorlist
  if [[ -n "${gConfig[${_section},MIRRORLIST]}" ]]; then
    # rewrite url to make sure we have a protocol
    _host=$(getUrlHost ${gConfig[${_section},MIRRORLIST]})
    _path=$(getUrlPath ${gConfig[${_section},MIRRORLIST]})
    printf -v _urlWithProto "%s%s" "${_host}" "${_path#/}"
    _urlWithProto="$(getEscapedStr ${_urlWithProto})"
    ${g_path_sed} -i.bak "s|%%${_section}_MIRRORLIST%%|mirrorlist=${_urlWithProto}|g" "${_tmpConfigFile}"
    unset _urlWithProto
    unset _path
    unset _host
  else
    ${g_path_sed} -i.bak "/%%${_section}_MIRRORLIST%%/d" "${_tmpConfigFile}"
  fi

  # enabled
  if [[ -n "${gConfig[${_section},ENABLED]}" ]]; then
    _escapedString="$(getEscapedStr ${gConfig[${_section},ENABLED]})"
    ${g_path_sed} -i.bak "s|%%${_section}_ENABLED%%|enabled=${_escapedString}|g" "${_tmpConfigFile}"
    unset _escapedString
  else
    ${g_path_sed} -i.bak "/%%${_section}_ENABLED%%/d" "${_tmpConfigFile}"
  fi
done
unset _section
unset _configSections
unset _repo100

if [[ -n "${g_path_outfile}" && ${g_use_stdout} -eq 0 ]]; then
  _resp=""
  _rslt=1
  #
  # copy to outfile, we use append here
  #
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Copying configuration to output file at path: ${g_path_outfile}"
  fi
  _resp=$({ ${g_path_cat} "${_tmpConfigFile}" >> "${g_path_outfile}"; } 2>&1)
  _rslt=$?
  if [[ ${_rslt} -ne 0 ]]; then
    echo "ABORTING. Unable to copy configuration to output file at path: ${g_path_outfile}"
    echo "Not sure what the problem is."
    exit 1
  fi
  unset _resp
else
  _resp=""
  #
  # echo to stdout
  #
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Copying configuration to stdout..."
  fi
  # capture stderr, let stdout through
  { _resp=$(${g_path_cat} ${_tmpConfigFile} 2>&1 1>&3-) ;} 3>&1
  unset _resp
fi

# remove tempfile and directory
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Removing temporary working directory: ${g_path_worktemp}"
fi
${g_path_rm} -rf "${g_path_worktemp}" &> /dev/null
if [[ $? -ne 0 ]]; then
  echo "ABORTING. Unable to remove temporary working directory: ${g_path_worktemp}"
  echo "Not sure what the problem is."
  exit 1
fi

exit 0
