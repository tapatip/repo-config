## repo-config
Update YUM repos to select reachable local mirrors. The repo-config utility can be called at system startup to reconfigure YUM to leverage local private mirrors and fallback to public mirrors based on server reachability. For an additional layer of security/reliability a REPO-KEY can be generated and added to local mirrors to verify their authenticity but also to verify their validity (just because a server is reachable at a particular URL does not necessarily mean it is a legitimate mirror).
Supports CentOS and Epel repos.

## Why?
When deploying a server remotely (e.g. in the Cloud) with a pre-defined image (AMI, Docker, etc.), ideally you'd like the system to bootstrap itself including self-configuration. Obviously, this is just something for your wish list if you don't deploy new servers often but when the volume of deployments increases or automated deployments are introduced the need for self configuration soon becomes a must-have. If the self configuration process requires updating distributed binaries via YUM, then it is often desirable to maintain a private repo mirror for reasons of efficiency, reliability, or security. When an image leaves DevOps reachability for deployment, there will likely be differences between the network configuration in the lab versus the configuration in the live environment. And this is where repo-config comes into the picture.

The repo-config utility can be triggered during system init to reconfigure the host's YUM configuration depending upon parameters passed on the command line and/or through a configuration file. There is support for specifying a list of one or more private repo mirrors to apply to a specific YUM target file and section; repo-config will attempt to connect to the specified mirrors to determine reachability and if one of the mirrors responds the YUM configuration will be updated to rewrite the `baseurl` value to point to the mirror. If no mirrors are reachable, repo-config will fall back to configure either a `baseurl` or `mirrorlist` value specified on the command line or from defaults.

## Installation
To install repo-config, just copy the entire script directory (including the templates, documentation and examples directories) to a suitable directory (like /usr/local/repo-config) as the superuser. Then, execute the following permissions changes:

```sh
>cd /path/to/install/directory (e.g. cd /usr/local)
>chown -R root:root repo-config
>chmod 755 repo-config/repo-config.sh
>ln -s repo-config.sh repo-config
>ln -s $PWD/repo-config.sh /usr/local/bin/repo-config
```

After the execute bit has been set on the repo-config.sh script, you will be able to call the script without having to invoke a shell first and then pass the script as an argument. A symbolic link is created in the /usr/local/bin directory so that the command becomes visible in your shell PATH.

**NOTE: If /usr/local/bin is not visible in your PATH, you will need to add it.**

**NOTE: repo-config requires BASH 4.2 or greater.**

### Installation for Dockerfile
To use repo-config with Dockerfile, you will need to copy the script into an executable place during the build process.

```sh
RUN mkdir -p /build/bin /build/tmp
WORKDIR /build
RUN curl -sSL -o tmp/repo-config.bz2 https://bitbucket.org/tapatip/repo-config/get/v1.0.4.tar.bz2 \
  && arc="tmp/repo-config.bz2"; file="repo-config.sh"; tmpl="templates/$" \
    file="$(tar tjf ${arc} | grep ${file})"; \
    tmpl="$(tar tjf ${arc} | grep ${tmpl})"; \
    dir="$(dirname ${file})" \
    && tar xjf $arc -C tmp $file && cp tmp/$file bin \
    && tar xjf $arc -C tmp $tmpl && cp -R tmp/$tmpl bin
```

The above line will extract only the script and template file to provide a minimal install for use in a Docker build process. The installed script is then called from a Dockerfile with its full path:

```sh
RUN bash -c '/build/bin/repo-config.sh -O \
  --private-mirrors 10.0.1.1/mirrors/epel/repo \
  --target CentOS-Base
  --repo-id base \
  --out-file /tmp/CentOS-Base.repo'
```

The above example will generate a `CentOS-Base.repo` file with a `baseurl` entry set to the indicated private mirror (if it is reachable) for the `base` section. If the mirror is unreachable, the default value for `baseurl` will be applied. **NOTE: THIS IS A BUG**

## Usage
When specifying the `-c | -- use-config` option and supplying an INI config file (see the examples directory), the file path can be provided as the final argument or its contents can be supplied to the repo-config command using the redirection operator `<`. For instance, both of the following invocations are supported:

```sh
>repo-config -c -d -O my-config-file.ini

-OR-

>repo-config -c -d -O < my-config-file.ini
```

For help, just invoke repo-config with the help `(-h | --help)` option:

```sh
>repo-config --help
```

Options include:

```sh
   -c, --use-config             Parse config file containing keys/vals
   -i, --in-file=path           Input file (path) instead of default
   -o, --out-file=path          Output file (path) instead of default
   -b, --repo-baseurl=URL       URL pointing to a baseurl
   -m, --repo-mirrorlist=URL    URL pointing to a mirrorlist server
   -n, --repo-id=name           Globally unique repo (section) id
                                  - "base"
                                  - "updates"
                                  - "extras"
                                  - "centosplus"
                                  - "base-source"
                                  - "updates-source"
                                  - "extras-source"
                                  - "centosplus-source"
                                  - "epel"
                                  - "epel-debuginfo"
                                  - "epel-source"
   -l, --repo-name=name         Human readable string describing the repo
   -p, --private-mirrors=list   Comma separated list of private mirrors
   -t, --target=name            Parent repo target:
                                  - "CentOS-Base" (default)
                                  - "CentOS-Sources"
                                  - "epel"
   -u, --update-yum             Update system files (on supported OS)
   -x, --repo-disable           Disable repo
   -y, --repo-enable            Enable repo
   -a, --repo-basearch          Replacement for YUM $basearch variable:
                                  - "i386"
                                  - "x86_64"
   -r, --repo-releasever        Replacement for YUM $releasever variable
   -s, --repo-staticvars        Generate output files with static values
   -k, --new-repokey            Generate a new repokey (REPO-KEY)
   -d, --debug                  Turn debugging on (increases verbosity)
   -f, --force                  Execute without user prompt
   -h, --help                   Show this message
   -v, --version                Output version of this script
   -z, --show-settings          Show final settings that will be applied
```

Be careful when specifying YUM variables (<u>basearch</u>, <u>releasever</u>) within command line arguments (--repo-baseurl, --repo-mirrorlist, and --private-mirrors). The shell will interpret any string preceded by the `$` as a variable; therefore, you need to wrap these types of arguments in single quotes and escape the dollar sign with a back slash character.

```sh
>repo-config.sh -n centosplus -p '10.0.1.2/mirror/centos/\$releasever/os/\$basearch/' -c examples/config_example_centos.ini
```

### Specifying repo options via command line
It's important to note that when specifying repo options from the command line (with the --repo flags) that you can only affect one repo target file and within that target only one section. To make changes to multiple sections within a target you will need to leverage support for a config file.

### Config file
In addition to (or instead of) command line configuration options you can also provide an INI format config file as input to the repo-config command. An example config file is provided in the utility's examples directory.

### Input file
The repo-config utility works its magic by replacing a small number of predefined tokens that appear in a configuration template file. The default template files are located in the utility's templates directory. You can create your own template file and specify its location using the `-i | --in-file` command line option.

### Output file
The generated configuration file will be copied to the default path (see _Show Settings_) or to the path specified by the `-o | --out-file` option. If the file exists, it will be truncated; if the file doesn't exist, it will be created. You can also send output to stdout by specifying the `-O | --out-stdout` option.

### Static YUM variable replacement
Repo URLs often have the YUM $basearch and/or $releasever variables embedded. These variables can also be specified when using command line arguments (--repo-baseurl, --repo-mirrorlist, and --private-mirrors). Embedded variables are problematic for testing mirror reachability since we can only test complete urls. When repo-config encounters these variables they will be replaced with default values (specify the --show-settings option to discover defaults) or values defined on the command line or through a config file.

Under normal operation the values will only be used to test reachability for repo mirrors; however enabling the static vars option (--repo-staticvars) will result in the generation of configuration files with static values embedded.

### Updating system settings
On supported operating systems (i.e. CentOS), YUM configuration files will be updated in place when the --update-yum option is used. On unsupported systems the option will be ignored and output will be sent to stdout.

### Show settings
To view merged values without doing anything else, simply add the `-z | --show-settings` option to your command line.

## Merging of values
The order in which configuration values will be merged is as follows left to right:

```
Defaults->INI file->Command line parameters
```

INI file parameters override defaults. Command line parameters override all previous values. To view merged values without doing anything else, simply add the `-z | --show-settings` option to your command line.

## Repo mirror authentication
You can authenticate a repo mirror by generating a SHA1 key named "REPO-KEY"and installing it at the root level of a repo mirror. When you include the key in the url of the repo mirror, repo-config will attempt to retrieve the REPO-KEY file from the repo mirror and compare it with the key provided. If they match, the reachability test will pass; if the keys don't match (or if the remote key is missing), the reachability test will fail. The REPO-KEY must be installed on the server within the directory path specified in the url; repo-config will search for a matching REPO-KEY from the root of the url to the end of the path string.

An example of a mirror url with a REPO-KEY:

```sh
MIRRORS="25d49bf53ce9f0a552d5fc293ea1b06dce367afd@192.168.1.1/mirror/centos/$releasever/updates/$basearch/"
```

Using a REPO-KEY is a good way to insure that you've provided a valid url for your repo mirror. A new REPO-KEY can be generated with the `-k | --new-repokey` option.

## Limitations
When specifying one or more repo mirrors, reachability will be tested based on the url supplied. As long as the server responds on doesn't return an error (e.g. 404) the test will succeed. There is no way to verify that the url points to a legitimate repo mirror and repo-config will not perform deeper tests to verify that, for instance, the mirror is an appropriate mirror for the repo. Such tests are beyond the scope of this utility.

## Compatibility
repo-config has been developed and tested on OSX and Linux. Supported Linux distributions currently include:

* [CoreOS](https://coreos.com/)
* [CentOS](http://www.centos.org/)

repo-config requires BASH 4.2.0 or higher.

## License
repo-config is licensed under the MIT open source license.
